<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>runc, as used in Docker and other products, allows AppArmor and
SELinux restriction bypass, and thus a malicious Docker image could
breach isolation.</p>

<p>This update carries SELinux-related fixes in the
golang-github-opencontainers-selinux library, that will be leveraged
in the upcoming runc security update.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.0~rc1+git20170621.5.4a2974b-1+deb10u1.</p>

<p>We recommend that you upgrade your golang-github-opencontainers-selinux packages.</p>

<p>For the detailed security status of golang-github-opencontainers-selinux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-github-opencontainers-selinux">https://security-tracker.debian.org/tracker/golang-github-opencontainers-selinux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3322.data"
# $Id: $

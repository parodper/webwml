#use wml::debian::translation-check translation="f8545f462fdd05fa94ece499d3a78fc01e46f8ed" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Zhuowei Zhang opdagede en fejl i EAP-autentifikationsklientkoden i 
strongSwan, en IKE-/IPsec-programsuite, der kunne gøre det muligt at omgå 
klienten og under nogle omstændigheder endda serverautentifikationen, eller 
kunne føre til lammelsesangreb.</p>

<p>Når der anvendes EAP-autentifikation (RFC 3748), indikeres en succesrig 
gennemførelse af autentifikationen af en EAP-Success-meddelelse sendt af 
serveren til klienten.  strongSwans EAP-klientkode håndteres tidligere 
EAP-Success-meddelelser på ukorrekt vis, enten ved at få IKE-dæmonen til at gå 
ned eller afslutte EAP-metoden for tidligt.</p>

<p>Slutresultatet er afhængigt af den anvendte opsætning, flere oplysninger 
finder man i opstrøms bulletin på 
<a href="https://www.strongswan.org/blog/2022/01/24/strongswan-vulnerability-(cve-2021-45079).html">\
https://www.strongswan.org/blog/2022/01/24/strongswan-vulnerability-(cve-2021-45079).html</a>.</p>

<p>I den gamle stabile distribution (buster), er dette problem rettet
i version 5.7.2-1+deb10u2.</p>

<p>I den stabile distribution (bullseye), er dette problem rettet i
version 5.9.1-1+deb11u2.</p>

<p>Vi anbefaler at du opgraderer dine strongswan-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende strongswan, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5056.data"

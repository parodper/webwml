#use wml::debian::translation-check translation="b1c4cad9758139bf39bbc354ba5a70c21270f43a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BlueZ, la pile du
protocole Bluetooth de Linux. Un attaquant pourrait provoquer un déni de
service (DoS) ou divulguer des informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8921">CVE-2019-8921</a>

<p>Fuite d'information de SDP ; la vulnérabilité réside dans le traitement
de SVC_ATTR_REQ par l'implémentation de SDP de BlueZ. En contrefaisant un
CSTATE, il est possible de piéger le serveur à renvoyer plus d'octets que
le tampon n'en contient en réalité, avec pour conséquence la divulgation de
données arbitraires du tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8922">CVE-2019-8922</a>

<p>Dépassement de tas de SDP ; cette vulnérabilité réside dans le traitement
par le protocole SDP des requêtes d'attribut également. En demandant un
très grand nombre d'attributs en même temps, un attaquant peut faire
déborder le tampon statique fourni pour contenir la réponse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41229">CVE-2021-41229</a>

<p>sdp_cstate_alloc_buf alloue de la mémoire qui sera toujours attachée à
la liste liée individuellement de cstates et ne sera pas libérée. Cela
provoquera une fuite de mémoire avec le temps. Les données peuvent être un
très grand objet, ce qui peut être provoqué par un attaquant envoyant en
continu des paquets sdp et cela peut faire planter le service du
périphérique cible.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 5.43-2+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bluez.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bluez, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bluez">\
https://security-tracker.debian.org/tracker/bluez</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2827.data"
# $Id: $

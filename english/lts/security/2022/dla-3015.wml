<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Fabian Vogt and Dominik Penner discovered that the Ark archive manager did not
sanitize extraction paths, which could result in maliciously crafted archives
with symlinks writing outside the extraction directory.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
4:16.08.3-2+deb9u1.</p>

<p>We recommend that you upgrade your ark packages.</p>

<p>For the detailed security status of ark please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ark">https://security-tracker.debian.org/tracker/ark</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3015.data"
# $Id: $

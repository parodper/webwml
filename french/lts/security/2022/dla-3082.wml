#use wml::debian::translation-check translation="0d4b17e0a19c56efbd16db41e834b08a1dd49503" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans Exim, un agent de transport de courriel, le traitement d'un courriel
peut provoquer un dépassement de tas dans certaines situations. Un attaquant
peut provoquer un déni de service (DoS) et éventuellement exécuter du code
arbitraire.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
4.92-8+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exim4.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de exim4, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/exim4">\
https://security-tracker.debian.org/tracker/exim4</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3082.data"
# $Id: $

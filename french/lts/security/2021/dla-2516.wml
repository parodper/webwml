#use wml::debian::translation-check translation="4139a249d5e4ddb427ae3c7ef8c33038092b7a8b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans la séparation des droits
de gssproxy provoquée par <em>gssproxy</em> ne déverrouillant pas
<em>cond_mutex</em> avant d’appeler <em>pthread_exit</em>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12658">CVE-2020-12658</a>

<p>gssproxy (c'est-à-dire gss-proxy) avant la version 0.8.3 ne déverrouillait
pas cond_mutex avant la fin de pthread dans gp_worker_main() dans gp_workers.c.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.5.1-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets  mandataire gs.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2516.data"
# $Id: $

#use wml::debian::translation-check translation="173191d3c56a66f3b7934359ed82d7d5424fa4ed" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème permettant à des attaquants
authentifiés de reconfigurer dnsmasq à l'aide d'une valeur contrefaite de
extra_dhcp_opts dans le service de réseau virtuel Neutron d’OpenStack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40085">CVE-2021-40085</a>

<p>Un problème a été découvert dans Neutron d’OpenStack avant les versions 16.4.1,
17.x avant 17.2.1 et 18.x avant 18.1.1. Des attaquants authentifiés peuvent
reconfigurer dnsmasq à l'aide d'une valeur contrefaite de extra_dhcp_opts.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2:9.1.1-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets neutron.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2781.data"
# $Id: $

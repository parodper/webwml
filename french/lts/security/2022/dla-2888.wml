#use wml::debian::translation-check translation="8147978dcfbd8d28c69a6d7d8e07010ed626a3e4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le pilote
binaire NVIDIA et les bibliothèques qui fournissent une accélération
matérielle optimisée. Cela pourrait conduire à un déni de service, à la
divulgation d'informations ou à une corruption de données.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 390.144-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets
nvidia-graphics-drivers.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de
nvidia-graphics-drivers, veuillez consulter sa page de suivi de sécurité à
l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nvidia-graphics-drivers">\
https://security-tracker.debian.org/tracker/nvidia-graphics-drivers</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2888.data"
# $Id: $

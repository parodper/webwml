<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were addressed in pacemaker, a cluster
resource manager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16877">CVE-2018-16877</a>

    <p>A flaw was found in the way pacemaker's client-server authentication was
    implemented. A local attacker could use this flaw, and combine it with
    other IPC weaknesses, to achieve local privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16878">CVE-2018-16878</a>

    <p>An insufficient verification inflicted preference of uncontrolled processes
    can lead to denial-of-service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25654">CVE-2020-25654</a>

    <p>An ACL bypass flaw was found in pacemaker. An attacker having a local
    account on the cluster and in the haclient group could use IPC
    communication with various daemons directly to perform certain tasks that
    they would be prevented by ACLs from doing if they went through the
    configuration.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.1.24-0+deb9u1.</p>

<p>We recommend that you upgrade your pacemaker packages.</p>

<p>For the detailed security status of pacemaker please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/pacemaker">https://security-tracker.debian.org/tracker/pacemaker</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2519.data"
# $Id: $

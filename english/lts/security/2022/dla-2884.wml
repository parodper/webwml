<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Wordpress, a web blogging
tool. They allowed remote attackers to perform SQL injection, run
unchecked SQL queries, bypass hardening, or perform Cross-Site
Scripting (XSS) attacks.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
4.7.22+dfsg-0+deb9u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>For the detailed security status of wordpress please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wordpress">https://security-tracker.debian.org/tracker/wordpress</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2884.data"
# $Id: $

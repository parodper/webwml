<define-tag pagetitle>Debian Installer Bookworm Alpha 1 release</define-tag>
<define-tag release_date>2022-09-22</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the first alpha release of the installer for Debian 12
<q>Bookworm</q>.
</p>

<p>
A number of changes were submitted by Debian Janitor and merged into
the many components that the installer is assembled out of, and
they're not documented individually since they're usually about
catching up with debhelper and other build-time best practices.
</p>

<p>
Special thanks to first-time contributors to the main debian-installer
source package:
</p>

<ul>
  <li>Youngtaek Yoon</li>
  <li>Sophie Brun</li>
  <li>Roland Clobus</li>
</ul>


<h2>Improvements in this release</h2>

<ul>
  <li>alsa-lib:
  <ul>
    <li>Install missing <code>/usr/share/alsa/ctl</code> dir in libasound2-udeb
    (<a href="https://bugs.debian.org/992536">#992536</a>).</li>
  </ul>
  </li>
  <li>anna:
  <ul>
    <li>Make it possible to install with a mismatched kernel (<a href="https://bugs.debian.org/998668">#998668</a>).</li>
  </ul>
  </li>
  <li>apt-setup:
  <ul>
    <li>Install ca-certificates when the detected protocol is https, so
    that the target can validate certificates (<a href="https://bugs.debian.org/1015887">#1015887</a>).</li>
  </ul>
  </li>
  <li>brltty:
  <ul>
    <li>Terminate main menu and debconf, otherwise the graphical
    versions remain behind and fill logs.</li>
    <li>Add a proper menu title.</li>
    <li>Enable screen reader in Cinnamon (<a href="https://bugs.debian.org/992169">#992169</a>).</li>
    <li>Disable liblouis and hid support in udeb.</li>
    <li>Update udev rules.</li>
    <li>Automatically shrink width to 80 columns, as that is much more
    convenient on Braille devices.</li>
  </ul>
  </li>
  <li>busybox:
  <ul>
    <li>Enable applets for the installer: awk, base64, less (<a href="https://bugs.debian.org/949626">#949626</a>),
    stty (<a href="https://bugs.debian.org/891806">#891806</a>).</li>
  </ul>
  </li>
  <li>cdebconf:
  <ul>
    <li>text: Make steps interruptible (<a href="https://bugs.debian.org/998424">#998424</a>).</li>
    <li>text: Use libreadline and history to allow choosing with arrows.</li>
  </ul>
  </li>
  <li>cdrom-detect:
  <ul>
    <li>Support detecting installer images on normal disks (<a href="https://bugs.debian.org/851429">#851429</a>).</li>
  </ul>
  </li>
  <li>choose-mirror:
  <ul>
    <li>Pull the mirror list from mirror-master.debian.org</li>
    <li>Sort deb.debian.org first, then ftp*.*.debian.org, then others.</li>
  </ul>
  </li>
  <li>console-setup:
  <ul>
    <li>Fix translation from X symbols to kernel symbols, for high Unicode
    code points (<a href="https://bugs.debian.org/968195">#968195</a>).</li>
  </ul>
  </li>
  <li>debian-installer:
  <ul>
    <li>Start speech synthesis automatically, after a 30-second timeout.</li>
    <li>Add support for multiple components in UDEB_COMPONENTS.</li>
    <li>Bump Linux kernel ABI to 5.19.0-1.</li>
    <li>Install bookworm, using bookworm udebs.</li>
    <li>Work around FTBFS on armel and mipsel, where libcrypto3-udeb
    depends on libatomic1, by copying files from the host.</li>
    <li>Harmonize UEFI (grub) and BIOS (syslinux) boot menus: some labels
    and also inclusion rules for the speech synthesis.</li>
    <li>Stop hardcoding the name of the distribution in syslinux's
    <code>menu.cfg</code></li>
    <li>Fix build reproducibility issues.</li>
  </ul>
  </li>
  <li>debootstrap:
  <ul>
    <li>Add (Debian) trixie as a symlink to sid.</li>
    <li>Add usr-is-merged to the required set on testing/unstable (see
    <a href="https://lists.debian.org/debian-devel-announce/2022/09/msg00001.html">Transition to usrmerge has started</a>).
    </li>
  </ul>
  </li>
  <li>espeakup:
  <ul>
    <li>Print the ALSA card number when choosing cards.</li>
    <li>Adjust languages approximations.</li>
    <li>Add support for mbrola voices, avoiding en1mrpa and us1mrpa.</li>
    <li>Install the mbrola voice used during the installation process,
    along with espeak-ng.</li>
  </ul>
  </li>
  <li>finish-install:
  <ul>
    <li>Improve understandability of reboot screen (<a href="https://bugs.debian.org/982640">#982640</a>).</li>
    <li>Enable screen reader in Cinnamon (<a href="https://bugs.debian.org/992169">#992169</a>).</li>
    <li>Create the legacy <code>/etc/mtab</code> symlinks with the same destination
    systemd uses.</li>
  </ul>
  </li>
  <li>freetype:
  <ul>
    <li>Build the udeb without librsvg.</li>
  </ul>
  </li>
  <li>gdk-pixbuf:
  <ul>
    <li>Build the PNG loader directly into the library.</li>
  </ul>
  </li>
  <li>glibc:
  <ul>
    <li>Adjust udeb for new layout (almost all symlinks are gone).</li>
  </ul>
  </li>
  <li>hw-detect:
  <ul>
    <li>Replace <code>/etc/pcmcia/</code> with <code>/etc/pcmciautils/</code> (<a href="https://bugs.debian.org/980271">#980271</a>).</li>
    <li>Remove experimental dmraid support.</li>
    <li>Install opal-prd package on OpenPOWER machines.</li>
  </ul>
  </li>
  <li>installation-report:
  <ul>
    <li>Include detected ALSA cards in hardware report.</li>
    <li>Reword template for saving logs (<a href="https://bugs.debian.org/683203">#683203</a>).</li>
  </ul>
  </li>
  <li>kmod:
  <ul>
    <li>Implement the generation of a less strict shlibs file.</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>Force more compressions modules into main installer package
    (<a href="https://bugs.debian.org/992221">#992221</a>).</li>
    <li>udeb: Add essiv to crypto-modules (<a href="https://bugs.debian.org/973378">#973378</a>).</li>
    <li>udeb: Add SCSI device handlers to multipath-modules (<a href="https://bugs.debian.org/989079">#989079</a>).</li>
    <li>udeb: Move crc64 to crc-modules and make scsi-core-modules depend
    on that.</li>
  </ul>
  </li>
  <li>localechooser:
  <ul>
    <li>Fix level detection (<a href="https://bugs.debian.org/1011254">#1011254</a>).</li>
    <li>Fix language detection when a 2-letter language is a prefix of a
    3-letter language.</li>
  </ul>
  </li>
  <li>lvm2:
  <ul>
    <li>Disable systemd usage in udeb (<a href="https://bugs.debian.org/1015174">#1015174</a>).</li>
  </ul>
  </li>
  <li>multipath-tools:
  <ul>
    <li>Improve support in the installer: ship a default config file and
    udev rules to make it easier to detect multipath devices.</li>
  </ul>
  </li>
  <li>nano:
  <ul>
    <li>Build the udeb against libncursesw6-udeb, as support for S-Lang
    was dropped (<a href="https://bugs.debian.org/976275">#976275</a>).</li>
  </ul>
  </li>
  <li>net-retriever:
  <ul>
    <li>Fix endianness support in netcfg_gateway_reachable (<a href="https://bugs.debian.org/1007929">#1007929</a>).</li>
    <li>Add support for preseeded pointopoint.</li>
    <li>Add support for fe80 addresses as gateway.</li>
  </ul>
  </li>
  <li>nvme:
  <ul>
    <li>Build nvme-cli-udeb, for use within the installer.</li>
  </ul>
  </li>
  <li>openssl:
  <ul>
    <li>Add ossl-modules to libcrypto's udeb.</li>
  </ul>
  </li>
  <li>os-prober:
  <ul>
    <li>Add Windows 11 detection.</li>
    <li>Add support for multiple initrd paths.</li>
    <li>Add Exherbo Linux detection (<a href="https://bugs.debian.org/755804">#755804</a>).</li>
    <li>Sort Linux kernels in reverse version order if no boot loader
    config file is found (<a href="https://bugs.debian.org/741889">#741889</a>).</li>
    <li>Detect ntfs3 (5.15+ kernels) in addition to ntfs and ntfs-3g.</li>
    <li>Fix regression introduced by calling `dmraid -r` once.</li>
    <li>Add detection for Alpine's initramfs files.</li>
    <li>Add reading <code>/usr/lib/os-release</code> as a fallback.</li>
  </ul>
  </li>
  <li>partman-auto:
  <ul>
    <li>Remove experimental dmraid support.</li>
  </ul>
  </li>
  <li>partman-base:
  <ul>
    <li>Remove experimental dmraid support.</li>
  </ul>
  </li>
  <li>partman-jfs:
  <ul>
    <li>Remove obsolete sanity check for JFS as boot or root
    filesystem.</li>
  </ul>
  </li>
  <li>readline:
  <ul>
    <li>Add libreadline8-udeb and readline-common-udeb, needed by the
    cdebconf text frontend (used for speakup-based accessibility).</li>
  </ul>
  </li>
  <li>rescue:
  <ul>
    <li>Detect situations where mounting <code>/usr</code> could be needed, and prompt
    about it (<a href="https://bugs.debian.org/1000239">#1000239</a>).</li>
    <li>Mount separate filesystems with mount options from fstab (needed
    e.g. with btrfs subvolumes).</li>
    <li>Fix various issues with mounting several separate filesystems.</li>
    <li>Refactor various mounting and unmounting operations for <code>/target</code>.</li>
  </ul>
  </li>
  <li>rootskel:
  <ul>
    <li>When reopening the Linux console, use tty1 instead of tty0, fixing
    Ctrl-c.</li>
  </ul>
  </li>
  <li>s390-dasd:
  <ul>
    <li>Stop passing deprecated -f option to dasdfmt (<a href="https://bugs.debian.org/1004292">#1004292</a>).</li>
  </ul>
  </li>
  <li>s390-tools:
  <ul>
    <li>Install hsci, used to show and control HiperSockets Converged
    Interfaces.</li>
  </ul>
  </li>
  <li>systemd:
  <ul>
    <li>Drop separate udeb build.</li>
    <li>udev-udeb: ship modprobe.d snippet to force scsi_mod.scan=sync in
    the installer.</li>
    <li>Bump systemd-timesyncd's priority to standard, to make sure it's
    installed by default (<a href="https://bugs.debian.org/986651">#986651</a>, <a href="https://bugs.debian.org/993947">#993947</a>).</li>
  </ul>
  </li>
  <li>wireless-regdb:
  <ul>
    <li>Remove regular files deployed by the installer (<a href="https://bugs.debian.org/1012601">#1012601</a>).</li>
  </ul>
  </li>
  <li>x11-xkb-utils:
  <ul>
    <li>Fix setxkbmap crash in the installer (<a href="https://bugs.debian.org/1010161">#1010161</a>).</li>
  </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-installer:
  <ul>
    <li>armhf: Add support for Bananapi_M2_Ultra (<a href="https://bugs.debian.org/982913">#982913</a>).</li>
    <li>armhf: Update MX53LOCO filename with newer u-boot.</li>
  </ul>
  </li>
  <li>flash-kernel:
  <ul>
    <li>Skip flash-kernel in all EFI systems.</li>
    <li>Add support for ODROID-C4, -HC4, -N2, -N2Plus (<a href="https://bugs.debian.org/982369">#982369</a>).</li>
    <li>Add Librem5r4 (Evergreen).</li>
    <li>Add SiFive HiFive Unmatched A00 (<a href="https://bugs.debian.org/1006926">#1006926</a>).</li>
    <li>Add BeagleV Starlight Beta board.</li>
    <li>Add Microchip PolarFire-SoC Icicle Kit.</li>
    <li>Add MNT Reform 2.</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>arm64: Include panel-edp in fb-modules udeb.</li>
    <li>arm64: Add nvmem-rockchip-efuse and phy-rockchip-inno-hdmi to
    fb-modules udeb.</li>
    <li>arm64: Add pwm-imx27, nwl-dsi, ti-sn65dsi86, imx-dcss, mxsfb,
    mux-mmio and imx8mq-interconnect to fb-modules udeb for the MNT
    Reform 2.</li>
    <li>mips*: Unify installer flavors.</li>
    <li>mips*: Add generic platform and remove 5kc-malta from 32-bit
    ports.</li>
  </ul>
  </li>
  <li>oldsys-preseed:
  <ul>
    <li>Drop support for arm*/ixp4xx and arm*/iop32x (no longer supported
    by the Linux kernel).</li>
  </ul>
</li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 30 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>

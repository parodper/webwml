#use wml::debian::translation-check translation="f5622964983ffb87fc2b46993333cbb1f4c78eac" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans rabbitmq-server, un
courtier logiciel de messages.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-4965">CVE-2017-4965</a>

<p>Plusieurs formulaires dans l’interface graphique de gestion de RabbitMQ
sont vulnérables à une attaque de script intersite (XSS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-4966">CVE-2017-4966</a>

<p>L’interface graphique de gestion de RabbitMQ enregistre les accréditations
d’utilisateur signées dans un stockage local du navigateur sans délai
d’expiration. Cela rend possible leur récupération en utilisant une attaque
chaînée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-4967">CVE-2017-4967</a>

<p>Plusieurs formulaires dans l’interface graphique de gestion de RabbitMQ
sont vulnérables à une attaque de script intersite (XSS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11281">CVE-2019-11281</a>

<p>La page de limites d’hôtes virtuels et l’interface graphique de gestion de
fédération ne nettoient pas correctement la saisie de l’utilisateur. Un
utilisateur distant malveillant ayant un accès administratif pourrait fabriquer
une attaque par script intersite pour obtenir un accès aux hôtes virtuels et
à l’information de gestion de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11287">CVE-2019-11287</a>

<p>L’en-tête HTTP « X-Reason » peut être exploité pour insérer une chaîne
de format Erlang malveillante qui élargit et consomme le tas, aboutissant
à un plantage du serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22116">CVE-2021-22116</a>

<p>Un utilisateur malveillant peut exploiter la vulnérabilité en envoyant
des messages AMQP malveillants à l’instance cible de RabbitMQ.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 3.6.6-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rabbitmq-server.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rabbitmq-server,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rabbitmq-server">\
https://security-tracker.debian.org/tracker/rabbitmq-server</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2710.data"
# $Id: $

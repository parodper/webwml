<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Alex Infuehr discovered a directory traversal vulnerability which could
result in the execution of Python script code when opening a malformed
document.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 1:5.2.7-1+deb9u5. In addition this update fixes a bug in the
validation of signed PDFs; it would display an incomplete status message
when dealing with a partial signature.</p>

<p>We recommend that you upgrade your libreoffice packages.</p>

<p>For the detailed security status of libreoffice please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libreoffice">\
https://security-tracker.debian.org/tracker/libreoffice</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4381.data"
# $Id: $

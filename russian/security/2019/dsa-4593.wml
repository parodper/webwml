#use wml::debian::translation-check translation="4b462ea192fc355c557625a4edd8b02668ca91dd" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>Было обнаружено, что freeimage, графическая библиотека, подвержена
двум следующим проблемам безопасности:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12211">CVE-2019-12211</a>

    <p>Переполнение буфера, вызываемое memcpy в PluginTIFF. Эта уязвимость может использоваться
    удалёнными злоумышленниками для вызова отказа в обслуживании или другого неопределённого
    влияния на безопасность с помощью специально сформированных данных в формате TIFF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12213">CVE-2019-12213</a>

    <p>Истощение стека, вызываемое нежелательной рекурсией в PluginTIFF. Эта
    уязвимость может использоваться удалёнными злоумышленниками для вызова отказа
    в обслуживании с помощью специально сформированных данных в формате TIFF.</p></li>

</ul>

<p>В предыдущем стабильном выпуске (stretch) эти проблемы были исправлены
в версии 3.17.0+ds1-5+deb9u1.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 3.18.0+ds2-1+deb10u1.</p>

<p>Рекомендуется обновить пакеты freeimage.</p>

<p>С подробным статусом поддержки безопасности freeimage можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/freeimage">\
https://security-tracker.debian.org/tracker/freeimage</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4593.data"

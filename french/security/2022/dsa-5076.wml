#use wml::debian::translation-check translation="abfb144498116750fd286e9c4c163a74c8c74491" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Des chercheurs en sécurité de JFrog Security et Ismail Aydemir ont
découvert deux vulnérabilités d'exécution de code à distance dans le moteur
de base de données Java SQL H2 qui peuvent être exploitées au moyen de
divers vecteurs d'attaque, plus particulièrement à travers la console de H2
et en chargeant des classes personnalisées à partir de serveurs distants en
utilisant JNDI. La console de H2 est un outil de développement et n'est
requise par aucune dépendance inverse dans Debian. Elle a été désactivée
dans les versions oldstable et stable. Il est recommandé aux développeurs
de bases de données d'utiliser au moins la version 2.1.210-1, actuellement
disponible dans Debian unstable.</p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 1.4.197-4+deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.4.197-4+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets h2database.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de h2database, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/h2database">\
https://security-tracker.debian.org/tracker/h2database</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5076.data"
# $Id: $

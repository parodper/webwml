#use wml::debian::translation-check translation="be8d801ea8c13810562f629890e94d1ceac2467a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans libde265, une implémentation au code
source ouvert du codec vidéo h.265, qui pouvaient aboutir à un déni de service
ou avoir un impact non précisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21599">CVE-2020-21599</a>

<p>libde265 v1.0.4 contenait un dépassement de tampon de tas dans la fonction
de265_image::available_zscan, qui pouvait être exploité à l'aide d'un fichier
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-35452">CVE-2021-35452</a>

<p>Un vulnérabilité de contrôle d’accès incorrect existait dans libde265
version 1.0.8 à cause d’une erreur de segmentation dans slice.cc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36408">CVE-2021-36408</a>

<p>libde265 version 1.0.8 contenait une utilisation de mémoire de tas après
libération dans intrapred.h quand un fichier était décodé en utilisant dec265.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36409">CVE-2021-36409</a>

<p>Il existait une assertion <q>scaling_list_pred_matrix_id_delta==1</q>
échouant dans sps.cc:925 dans libde265 version 1.0.8 lors du décodage d’un
fichier. Elle permettait à des attaquants de provoquer un déni de service (DoS)
en exécutant l’application avec un fichier contrefait ou éventuellement avoir un
impact non précisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36410">CVE-2021-36410</a>

<p>Un dépassement de tampon de pile existait dans libde265 version 1.0.8 à
l’aide de fallback-motion.cc dans la fonction put_epel_hv_fallback lors de
l’exécution d’un programme dec265.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36411">CVE-2021-36411</a>

<p>Un problème a été découvert dans libde265 version 1.0.8 à cause d’un contrôle
d’accès incorrect. Une erreur de segmentation causée par un accès en lecture de
mémoire dans la fonction derive_boundaryStrength de deblock.cc se produisait.
La vulnérabilité causait une erreur de segmentation et un plantage d'application,
qui conduisait à un déni de service distant.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 1.0.3-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libde265.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libde265,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libde265">\
https://security-tracker.debian.org/tracker/libde265</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3240.data"
# $Id: $

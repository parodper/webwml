#use wml::debian::translation-check translation="4cac3796a6e5173dab497f55cd13f91d944359f2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Curl, une bibliothèque côté
client de transfert par URL d'utilisation facile, qui pouvaient avoir pour
conséquences un déni de service ou la divulgation d'informations.</p>

<p>Cette mise à jour révise le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-27782">CVE-2022-27782</a> \
publié dans la DLA-3085-1.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 7.64.0-4+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3288.data"
# $Id: $

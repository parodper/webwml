#use wml::debian::translation-check translation="45376eb21b8bfd7700d66cabdb13abc517ee51b4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Helmut Grohne a découvert un défaut dans Heimdal, une implémentation de
Kerberos 5 qui vise à être compatible avec Kerberos du MIT. Le rétroportage
des corrections pour
<a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a>
inversait accidentellement d'importantes comparaisons de mémoire dans les
gestionnaires de vérification d'intégrité arcfour-hmac-md5 et rc4-hmac pour
gssapi, avec par conséquence la validation incorrecte de codes d'intégrité
de message.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 7.7.0+dfsg-2+deb11u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets heimdal.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de heimdal, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5344.data"
# $Id: $

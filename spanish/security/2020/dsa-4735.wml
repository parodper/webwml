#use wml::debian::translation-check translation="f316e0ce25840c6590881cb5b3cec62cc137c07d"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el gestor de arranque GRUB2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10713">CVE-2020-10713</a>

    <p>Se encontró un defecto en el código que analiza sintácticamente grub.cfg que permite romper
    el arranque seguro UEFI y cargar código arbitrario. Puede encontrar detalles en
    <a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14308">CVE-2020-14308</a>

    <p>Se descubrió que grub_malloc no valida el tamaño de la
    asignación, permitiendo un desbordamiento aritmético y, como consecuencia, un desbordamiento
    de memoria dinámica («heap»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14309">CVE-2020-14309</a>

    <p>Un desbordamiento de entero en grub_squash_read_symlink puede dar lugar a un desbordamiento de memoria dinámica («heap»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14310">CVE-2020-14310</a>

    <p>Un desbordamiento de entero en read_section_from_string puede dar lugar a un desbordamiento de memoria dinámica («heap»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14311">CVE-2020-14311</a>

    <p>Un desbordamiento de entero en grub_ext2_read_link puede dar lugar a un desbordamiento
    de memoria dinámica («heap»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15706">CVE-2020-15706</a>

    <p>script: evita un «uso tras liberar» al redefinir una función durante
    la ejecución.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15707">CVE-2020-15707</a>

    <p>Se encontró un defecto de desbordamiento de entero en la gestión del tamaño de initrd.</p></li>
</ul>

<p>Se puede encontrar información más detallada en
<a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot</a></p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.02+dfsg1-20+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de grub2.</p>

<p>Para información detallada sobre el estado de seguridad de grub2, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/grub2">https://security-tracker.debian.org/tracker/grub2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4735.data"

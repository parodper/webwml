<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in OpenSSL, a Secure
Sockets Layer toolkit, which may result in incomplete encryption, side
channel attacks, denial of service or information disclosure.</p>

<p>Additional details can be found in the upstream advisories at
<a href="https://www.openssl.org/news/secadv/20220705.txt">https://www.openssl.org/news/secadv/20220705.txt</a> and
<a href="https://www.openssl.org/news/secadv/20230207.txt">https://www.openssl.org/news/secadv/20230207.txt</a></p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1.1.1n-0+deb11u4.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5343.data"
# $Id: $

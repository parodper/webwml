<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a a remote code execution vulnerability in
<a href="https://www.mumble.info/">Mumble</a>, a VoIP client commonly used for
group chats. The exploit could have been been triggered by a maliciously
crafted URL on the server list.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27229">CVE-2021-27229</a>

    <p>Mumble before 1.3.4 allows remote code execution if a victim navigates
    to a crafted URL on a server list and clicks on the Open Webpage
    text.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.2.18-1+deb9u2.</p>

<p>We recommend that you upgrade your mumble packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2562.data"
# $Id: $

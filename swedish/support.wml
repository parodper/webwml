#use wml::debian::template title="Användarsupport" MAINPAGE="true"
#use wml::debian::translation-check translation="c7a64739b495b3bd084b972d820ef78fc30a3f8a"
#use wml::debian::toc

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#irc">IRC (realtidssupport)</a></li>
  <li><a href="#mail_lists">Sändlistor</a></li>
  <li><a href="#usenet">Nyhetsgrupper på Usenet</a></li>
  <li><a href="#forums">Debians användarforum</a></li>
  <li><a href="#maintainers">Hur man kontaktar paketansvariga</a></li>
  <li><a href="#bts">Felspårningssystemet</a></li>
  <li><a href="#release">Kända problem</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debiansupport erbjuds av
en grupp frivilliga. Om denna gemenskapsdrivna support inte fyller dina behov
och du inte kan hitta svaret i vår <a href="doc/">dokumentation</a>,
kan du hyra en <a href="consultants/">konsult</a> för att besvara dina frågor,
eller för att underhålla eller lägga till ytterligare funktionalitet till ditt
Debiansystem.</p>
</aside>

<h2><a id="irc">IRC (realtidssupport)</a></h2>

<p>
<a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) är ett fantastiskt sätt
att chatta med folk från hela världen i realtid. Det är ett textbaserat
chatsystem för direktmeddelanden. På IRC kan du gå in i chatrum (så kallade kanaler)
eller så kan du direkt chatta med individer via privata meddelanden.
</p>

<p>
IRC-kanaler som är dedicerade till Debian kan hittas på <a
href="https://www.oftc.net/">OFTC</a>. För en fullständig lista på Debiankanaler
vänligen se vår <a href="https://wiki.debian.org/IRC">Wiki</a>. Du kan även
använda en <a
href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">sökmotor</a>
för att leta efter Debianrelaterade kanaler.
</p>

<h3>IRC-klienter</h3>

<p>
För att ansluta till IRC-nätverket kan du antingen använda OFTC's <a
href="https://www.oftc.net/WebChat/">WebChat</a> i din föredragna webbläsare
eller installera en klient på din dator. Det finns en mängd olika klienter där
ute, några med grafiskt gränssnitt, och några för kommandoraden. Några
populära IRC-klienter har paketerats i Debian, exempelvis:
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a> (textläge)</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (textläge)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat<a> (GTK)</li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul>

<p>
Debianwikin erbjuder en mer fullständig <a
href="https://wiki.debian.org/IrcClients">lista på IRC-klienter</a> som finns
tillgängliga som Debianpaket.
</p>

<h3>Anslut till nätverket</h3>

<p>
När du väl har en klient installerad, behöver du säga till den att
ansluta till servern. I de flesta fall kan du göra detta genom att skriva:
</p>

<pre>
/server irc.debian.org
</pre>

<p>Värdnamnet irc.debian.org är ett alias för irc.oftc.net. I några klienter
(så som irssi) kommer du vara tvungen att skriva följande istället:</p>

<pre>
/connect irc.debian.org
</pre>


<h3>Gå med i en kanal</h3>

<p>
När du väl har anslutit dig går du in på kanalen <code>#debian</code>
genom att skriva följande kommando:
</p>

<pre>
/join #debian
</pre>

<p>En svenskspråkig kanal med liknande publik är <code>#debian.se</code>.</p>

<p>
Observera att grafiska klienter såsom HexChat eller Konversation ofta har en
knapp eller en meny för att ansluta till servrar och kanaler.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">Läs vår IRC FAQ</a></button></p>


<h2><a id="mail_lists">Sändlistor</a></h2>

<p>
Mer än tusen aktiva <a href="intro/people.en.html#devcont">utvecklare</a>
finns utspridda över hela världen som arbetar på Debian på deras fritid och
i deras egna tidszoner. Därför kommunicerar vi primärt via e-post. På samma
sätt sker de flesta av konversationerna mellan Debianutvecklare och användare
på olika <a href="MailingLists/">sändlistor</a>:
</p>

<ul>
  <li>För användarstöd på engelska, vänligen kontakta sändlistan <a href="https://lists.debian.org/debian-user/">debian-user</a>.</li>
  <li>För användarstöd på svenska, vänligen kontakta sändlistan <a href="https://lists.debian.org/debian-user-swedish/">debian-user-swedish</a>.</li>
  <li>For användarstöd på andra språk, vänligen se <a href="https://lists.debian.org/users.html">översikten</a> över andra användarsändlistor.</li>
</ul>

<p>
Du kan bläddra igenom vårt <a href="https://lists.debian.org/">sändlistearkiv</a> eller
<a href="https://lists.debian.org/search.html">söka</a> igenom arkiven utan
att prenumerera.
</p>


<p>
Självklart finns det en mängd andra sändlistor, dedikerade till någon aspekt
av Linux-ekosystemet och inte Debian-specifika. Använd din favoritsökmotor
för att hitta den mest lämpliga listan för ditt ändamål.
</p>

<h2><a id="usenet">Usenet Newsgroups</a></h2>
 
<p>
Många av våra <a href="#mail_lists">sändlistor</a> kan även läsas som
diskussionsgrupper (<span lang="en">news</span>) i hierarkin
<kbd>linux.debian.*</kbd>.
</p>

<h2><a id="forums">Debian Användarforum</h2>

<p><a href="https://forums.debian.net">Debian User Forums</a>
är en webbportal där tusentals användare diskuterar Debian-relaterade ämnen,
ställer frågor, och hjälper varandra genom att besvara dessa. Du kan läsa alla
diskussionsforum utan att registrera dig. Men om du vill delta i diskussionerna
och publicera dina egna poster måste du registrera dig och logga in.
</p>

<h2><a id="maintainers">Hur man kontaktar paketansvariga</a></h2>

<p>
Kortfattat finns det två sätt att kontakta ansvariga för paket i Debian:
</p>

<ul>
  <li>Om du vill rapportera ett fel, skapa en <a href="Bugs/Reporting">felrapport</a>; den paketansvarige får automatiskt en kopia av din felrapport.</li>
  <li>Om du helt enkelt vill skicka en e-post till paketansvarige, använd det speciella e-postalias som finns för varje paket:<br>
      &lt;<em>paketnamn</em>&gt;@packages.debian.org</li>
</ul>

<h2><a id="bts">Felspårningssystemet</a></h2>

<p>
Debiandistributionen har sitt eget <a href="Bugs/">felspårningssystem</a> med
fel som har rapporterats av användare och utvecklare. Varje fel har ett unikt
nummer och arkiveras fram tills det är markerat som löst.
Det finns flera sätt att rapportera fel:
</p>

<ul>
  <li>Det rekommenderade sättet är att använda Debianpaketet <em>reportbug</em>.</li>
  <li>Alternativt kan du skicka e-post så som det beskrivs på <a href="Bugs/Reporting">denna sida</a>.</li>
</ul>



<toc-add-entry name="release" href="releases/stable/">Kända problem</toc-add-entry>

<p>Begränsningar och allvarliga problem i den aktuella stabila utgåvan (om
sådana finns) beskrivs i <a href="releases/stable/">utgåvesidorna</a>.</p>

<p>Uppmärksamma speciellt <a href="releases/stable/releasenotes">\
versionsfakta</a> samt <a href="releases/stable/errata">errata</a>.</p>

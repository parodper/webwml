#use wml::debian::translation-check translation="0489e1b952f47155cfd52286f4b52319011dbc06" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Trail de Bits a utilisé les outils automatiques de découverte de vulnérabilité
développés par « Cyber Grand Challenge » de DARPA pour inspecter zlib. Comme
rsync, un outil rapide, polyvalent, de copie de fichiers distants (ou locaux),
utilise une copie embarquée de zlib, ces problèmes sont aussi présents dans
rsync.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9840">CVE-2016-9840</a>

<p>Dans le but d’éviter un comportement indéfini, suppression de l’optimisation
du pointeur de décalage, car ce n’est pas conforme avec la norme C.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9841">CVE-2016-9841</a>

<p>Utilisation seule d’une post-incrémentation pour être conforme avec la
norme du langage C.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9842">CVE-2016-9842</a>

<p>Dans le but d’éviter un comportement indéfini, ne pas modifier les valeurs
négatives, car ce n’est pas conforme avec la norme du langage C.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9843">CVE-2016-9843</a>

<p>Dans le but d’éviter un comportement indéfini, ne pas pré-décrémenter un
pointeur dans le calcul CRC petit boutiste, car ce n’est pas conforme avec la
norme du langage C.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5764">CVE-2018-5764</a>

<p>Empêchement pour des attaquants distants d’être capable de contourner le
mécanisme de protection argument-vérification en ignorant --protect-args lorsque
déjà envoyé par le client.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.1.1-3+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets rsync.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>


</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1725.data"
# $Id: $

#use wml::debian::translation-check translation="cbe9cd7fab7b8b8992562b1757c8d4d429b5c67c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichiers SMB/CIFS, d'impression et d'authentification pour Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2031">CVE-2022-2031</a>

<p>Luke Howard a signalé que les utilisateurs du serveur AD de Samba peuvent
contourner certaines restrictions associées au changement de mot de passe.
Un utilisateur à qui il a été demandé de changer son mot de passe peut
exploiter cela pour obtenir et utiliser des tickets d'autres services.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32742">CVE-2022-32742</a>

<p>Luca Moro a signalé qu'un client SMB1 doté d'un accès en écriture vers un
partage peut provoquer la divulgation du contenu de la mémoire du serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32744">CVE-2022-32744</a>

<p>Joseph Sutton a signalé que les utilisateurs du serveur AD de Samba
peuvent contrefaire des requêtes de changement de mot de passe pour
n'importe quel utilisateur avec pour conséquence une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32745">CVE-2022-32745</a>

<p>Joseph Sutton a signalé que les utilisateurs du serveur AD de Samba
peuvent planter le processus du serveur avec une requête add ou modify LDAP
contrefaite pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32746">CVE-2022-32746</a>

<p>Joseph Sutton et Andrew Bartlett ont signalé que les utilisateurs du
serveur AD de Samba peuvent provoquer une utilisation de mémoire après
libération dans le processus du serveur avec une requête add ou modify LDAP
contrefaite pour l'occasion.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2:4.13.13+dfsg-1~deb11u5. La correction du
<a href="https://security-tracker.debian.org/tracker/CVE-2022-32745">CVE-2022-32745</a>
nécessite la mise à jour vers ldb 2:2.2.3-2~deb11u2 pour corriger le défaut.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5205.data"
# $Id: $

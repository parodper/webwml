<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that PgBouncer, a PostgreSQL connection pooler, was
susceptible to an arbitrary SQL injection attack if a man-in-the-middle
could inject data when a connection using certificate authentication is
established.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.7.2-2+deb9u1.</p>

<p>We recommend that you upgrade your pgbouncer packages.</p>

<p>For the detailed security status of pgbouncer please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pgbouncer">https://security-tracker.debian.org/tracker/pgbouncer</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2922.data"
# $Id: $

#use wml::debian::translation-check translation="2c6f9b231c01f509a050624887ccd656065a82b0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans glib2.0,
une bibliothèque d'utilitaires généralistes pour l'environnement GNOME.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27218">CVE-2021-27218</a>

<p>Si g_byte_array_new_take() était appelé avec un tampon de 4 Go ou plus
sur une plateforme 64 bits, la longueur pourrait être tronquée modulo
2**32, provoquant une troncature imprévue de longueur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27219">CVE-2021-27219</a>

<p>La fonction g_bytes_new présente un dépassement d'entier sur les
plateformes 64 bits à cause d'un forçage de 64 bits à 32 bits. Le
dépassement pouvait éventuellement mener à une corruption de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28153">CVE-2021-28153</a>

<p>Quand g_file_replace() est employé avec G_FILE_CREATE_REPLACE_DESTINATION
pour remplacer un chemin qui est un lien symbolique bancal, il crée aussi
incorrectement la cible du lien symbolique sous la forme d'un fichier vide.
Cela pouvait avoir probablement une incidence sur la sécurité si le
lien symbolique est contrôlé par l'attaquant. (Si le chemin est un lien
symbolique vers un fichier déjà existant, alors le contenu de ce fichier
demeure inchangé de façon correcte.)</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.50.3-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets glib2.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de glib2.0, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/glib2.0">\
https://security-tracker.debian.org/tracker/glib2.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3044.data"
# $Id: $

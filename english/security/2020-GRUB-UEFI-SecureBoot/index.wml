#use wml::debian::template title="GRUB2 UEFI SecureBoot vulnerability - 'BootHole'"

<p>
Developers in Debian and elsewhere in the Linux community have
recently become aware of a severe problem in the GRUB2 bootloader that
allows a bad actor to completely circumvent UEFI Secure Boot. The full
details of the problem are described
in <a href="$(HOME)/security/2020/dsa-4735">Debian Security Advisory
4735</a>. The aim of this document is to explain the consequences of
this security vulnerability, and what steps have been taken to address
it.
</p>

<ul>
  <li><b><a href="#what_is_SB">Background: What is UEFI Secure Boot?</a></b></li>
  <li><b><a href="#grub_bugs">Multiple GRUB2 bugs found</a></b></li>
  <li><b><a href="#linux_bugs">Linux bugs found too</a></b></li>
  <li><b><a href="#revocations">Key revocations needed to fix the Secure Boot chain</a></b></li>
  <li><b><a href="#revocation_problem">What are the effects of key revocation?</a></b></li>
  <li><b><a href="#package_updates">Updated packages</a></b>
  <ul>
    <li><b><a href="#grub_updates">1. GRUB2</a></b></li>
    <li><b><a href="#linux_updates">2. Linux</a></b></li>
    <li><b><a href="#shim_updates">3. Shim</a></b></li>
    <li><b><a href="#fwupdate_updates">4. Fwupdate</a></b></li>
    <li><b><a href="#fwupd_updates">5. Fwupd</a></b></li>
  </ul></li>
  <li><b><a href="#buster_point_release">Debian 10.5 (<q>buster</q>)
        point release, updated installation and live
        media</a></b></li>
  <li><b><a href="#more_info">More information</a></b></li>
</ul>

<h1><a name="what_is_SB">Background: What is UEFI Secure Boot?</a></h1>

<p>
UEFI Secure Boot (SB) is a verification mechanism for ensuring that
code launched by a computer's UEFI firmware is trusted. It is designed
to protect a system against malicious code being loaded and executed
early in the boot process, before the operating system has been
loaded.
</p>

<p>
SB works using cryptographic checksums and signatures. Each
program that is loaded by the firmware includes a signature and a
checksum, and before allowing execution the firmware will verify that
the program is trusted by validating the checksum and the
signature. When SB is enabled on a system, any attempt to execute an
untrusted program will not be allowed. This stops unexpected /
unauthorised code from running in the UEFI environment.
</p>

<p>
Most x86 hardware comes from the factory pre-loaded with Microsoft
keys. This means the firmware on these systems will trust binaries
that are signed by Microsoft. Most modern systems will ship with SB
enabled - they will not run any unsigned code by default, but it is
possible to change the firmware configuration to either disable SB or
to enrol extra signing keys.
</p>

<p>
Debian, like many other Linux-based operating systems, uses a program
called shim to extend that trust from the firmware to the other
programs that we need to be secured during early boot: the GRUB2
bootloader, the Linux kernel and firmware update tools (fwupd and
fwupdate).
</p>

<h1><a name="grub_bugs">Multiple GRUB2 bugs found</a></h1>

<p>
Unfortunately, a serious bug has been found in the GRUB2 bootloader
code which reads and parses its configuration (grub.cfg). This bug
breaks the chain of trust; by exploiting this bug, it is possible to
break out of the secured environment and load non-signed programs
during early boot. This vulnerability was discovered by researchers at
Eclypsium and given the name
<b><a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">BootHole</a></b>.
</p>

<p>
Instead of just fixing that one bug, developers have been prompted to
do an in-depth audit of GRUB2's source code. It would have been
irresponsible to fix one major flaw without also looking for others! A
team of engineers have worked together for several weeks to identify
and repair a range of further issues. We have found a few places where
internal memory allocations could overflow given unexpected inputs,
several more places where integer overflow in math calculations could
cause trouble, and a few places where memory might be used after
freeing it. Fixes for all of these have been shared and tested across
the community.
</p>

<p>
Again, see <a href="$(HOME)/security/2020/dsa-4735">Debian Security
Advisory 4735</a> for a full list of the issues found.
</p>


<h1><a name="linux_bugs">Linux bugs found too</a></h1>

<P>
While discussing the GRUB2 flaws, developers also spoke about two
bypasses recently found and fixed by Jason A. Donenfeld (zx2c4)
(<a href="https://git.zx2c4.com/american-unsigned-language/tree/american-unsigned-language.sh">1</a>, <a href="https://git.zx2c4.com/american-unsigned-language/tree/american-unsigned-language-2.sh">2</a>)
where Linux might also allow Secure Boot bypass. These both allow root
to replace ACPI tables on a locked-down system when this should not be
permitted. Fixes have already been released for those issues.
</p>

<h1><a name="revocations">Key revocations needed to fix the Secure Boot chain</a></h1>

<p>
Debian and other operating system providers will obviously be <a
href="#package_updates">releasing fixed versions</a> of GRUB2 and
Linux. However, that cannot be a complete fix for the problems seen
here. Malicious actors would still be able to use older vulnerable
versions of each to be able to work around Secure Boot.
</p>

<p>
To stop that, the next step will be for Microsoft to block those
insecure binaries to stop them being run under SB. This is achieved
using the <b>DBX</b> list, a feature of the UEFI Secure Boot
design. All of the Linux distributions shipping with Microsoft-signed
copies of shim have been asked to provide details of the binaries or
keys involved to facilitate this process. The <a
href="https://uefi.org/revocationlistfile">UEFI revocation list
file</a> will be updated to include that information. At <b>some</b>
future point, systems will start to use that updated list and will
refuse to run the vulnerable binaries under Secure Boot.
</p>

<p>
The <i>exact</i> timeline for that change being deployed is not yet
clear. BIOS/UEFI vendors will include the new revocation list in new
firmware builds for new hardware at some point. Microsoft <b>may</b>
also issue updates to existing systems via Windows Update. Some Linux
distributions may issue updates via their own security updates
process. Debian does not <b>yet</b> do this, but we are looking into
it for the future.
</p>

<h1><a name="revocation_problem">What are the effects of key revocation?</a></h1>

<p>
Most vendors are wary about automatically applying updates which
revoke keys used for Secure Boot. Existing SB-enabled software
installations may suddenly refuse to boot altogether, unless the user
is careful to also install all the needed software updates as
well. Dual-boot Windows/Linux systems may suddenly stop booting
Linux. Old installation and live media will of course also fail to
boot, potentially making it harder to recover systems.
</p>

<p>
There are two obvious ways to fix a non-booting system like this:
</p>

<ul>
  <li>Reboot into <q>rescue</q> mode
    using <a href="#buster_point_release">newer installation media</a>, and
    apply the needed updates that way; or</li>
  <li>Temporarily disable Secure Boot to regain access to the system,
    apply updates then re-enable it.</li>
</ul>

<p>
These may both sound like simple options, but each could be very
time-consuming for users with multiple systems. Also be aware that
enabling or disabling Secure Boot needs direct machine access, by
design. It is normally <b>not</b> possible to change that
configuration outside of the computer's firmware setup. Remote server
machines may need special care here for exactly this reason.
</p>

<p>
For these reasons, it is strongly recommended that <b>all</b> Debian
users be careful to install all
the <a href="#package_updates">recommended updates</a> for their
systems as soon as possible, to reduce the chances of problems in
future.
</p>

<h1><a name="package_updates">Updated packages</a></h1>

<p>
<b>Note:</b> Systems running Debian 9 (<q>stretch</q>) and older
will <b>not</b> necessarily receive updates here, as Debian 10
(<q>buster</q>) was the first Debian release to include support for UEFI
Secure Boot.
</p>

<p>The signed versions of all packages here have been updated, even if
no other changes were needed. Debian has had to generate a new signing
key/certificate for its own Secure Boot packages. The old certificate
was labelled <q>Debian Secure Boot Signer</q> (fingerprint
<code>f156d24f5d4e775da0e6a9111f074cfce701939d688c64dba093f97753434f2c</code>);
the new certificate is <q>Debian Secure Boot Signer 2020</q>
(<code>3a91a54f9f46a720fe5bbd2390538ba557da0c2ed5286f5351fe04fff254ec31</code>). </p>

<p>
There are five source packages in Debian that will be updated due to
the UEFI Secure Boot changes described here:
</p>

<h2><a name="grub_updates">1. GRUB2</a></h2>

<p>
Updated versions of Debian's GRUB2 packages are available now via the
debian-security archive for the stable Debian 10 release
(<q>buster</q>). Fixed versions will be in the normal Debian archive
for development versions of Debian (unstable and testing) very soon.
</p>

<h2><a name="linux_updates">2. Linux</a></h2>

<p>
Updated versions of Debian's linux packages are available now via
buster-proposed-updates for the stable Debian 10 (<q>buster</q>)
release and will be included with the upcoming 10.5 point
release. New packages are also in the Debian archive for development
versions of Debian (unstable and testing). We hope to have fixed
packages uploaded for buster-backports soon also.
</p>

<h2><a name="shim_updates">3. Shim</a></h2>

<p>
Due to the way that Debian's Secure Boot key management works, Debian
does <b>not</b> need to revoke its existing Microsoft-signed shim
packages. However, the signed versions of the shim-helper packages
needed rebuilding to use the new signing key.
</p>

<p>
Updated versions of Debian's shim packages are available now via
buster-proposed-updates for the stable Debian 10 (<q>buster</q>)
release and will be included with the upcoming 10.5 point
release. New packages are also in the Debian archive for development
versions of Debian (unstable and testing).
</p>

<h2><a name="fwupdate_updates">4. Fwupdate</a></h2>

<p>
Updated versions of Debian's fwupdate packages are available now via
buster-proposed-updates for the stable Debian 10 (<q>buster</q>)
release and will be included with the upcoming 10.5 point
release. fwupdate had already been removed from unstable and testing
a while ago in favour of fwupd.
</p>

<h2><a name="fwupd_updates">5. Fwupd</a></h2>

<p>
Updated versions of Debian's fwupd packages are available now via
buster-proposed-updates for the stable Debian 10 (<q>buster</q>)
release and will be included with the upcoming 10.5 point
release. New packages are also in the Debian archive for development
versions of Debian (unstable and testing).
</p>

<h1><a name="buster_point_release">Debian 10.5 (<q>buster</q>) point
release, updated installation and live media</a></h1>

<p>
All of the fixes described here are targeted for inclusion in the
Debian 10.5 (<q>buster</q>) point release, due for release on 1st
August 2020. 10.5 would therefore be a good choice for users looking
for Debian installation and live media. Earlier images may not work
with Secure Boot in future as revocations roll out.
</p>

<h1><a name="more_info">More information</a></h1>

<p>
Much more information about Debian's UEFI Secure Boot setup is in the
Debian wiki -
see <a href="https://wiki.debian.org/SecureBoot">https://wiki.debian.org/SecureBoot</a>.</p>

<p>
Other resources on this topic include:
</p>

<ul>
  <li><a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">Eclypsium
      <q>BootHole</q> article</a> describing the weaknesses found</li>
  <li><a href="https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/ADV200011">Microsoft
      Guidance for Addressing Security Feature Bypass in GRUB</a></li>
  <li><a href="https://wiki.ubuntu.com/SecurityTeam/KnowledgeBase/GRUB2SecureBootBypass">Ubuntu
      KnowledgeBase article</a></li>
  <li><a href="https://access.redhat.com/security/vulnerabilities/grub2bootloader">Red
      Hat vulnerability article</a></li>
  <li><a href="https://www.suse.com/c/suse-addresses-grub2-secure-boot-issue/">SUSE
      vulnerability article</a></li>
</ul>

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Insufficient validation of data from the X server in libxrandr
before v1.5.0 can cause out of boundary memory writes and integer
overflows.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.3.2-2+deb7u2.</p>

<p>We recommend that you upgrade your libxrandr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-660.data"
# $Id: $

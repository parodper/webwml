#use wml::debian::translation-check translation="9e15d28daa5f346577a0d14e18d9f8a11f823f86" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une série de vulnérabilités de déni de
service dans Pygments, une bibliothèque populaire de coloration syntaxique pour
Python.</p>

<p>Un certain nombre d’expressions rationnelles étaient sujettes à une complexité
cubique ou exponentielle dans des cas extrêmes qui pouvait causer un déni de
service distant (DoS) lors de la fourniture d’une entrée malveillante.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27291">CVE-2021-27291</a>

<p>Dans pygments, versions 1.1 et plus, corrigés dans 2.7.4, les analyseurs
lexicaux utilisés pour vérifier les langages de programmation s’appuient
fortement sur les expressions rationnelles. Certaines ont une complexité
cubique ou exponentielle dans des cas extrêmes et sont vulnérables à une
attaque ReDoS. En contrefaisant une entrée, un attaquant malveillant peut
provoquer un déni de service.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.2.0+dfsg-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pygments.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2600.data"
# $Id: $

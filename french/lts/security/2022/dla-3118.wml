#use wml::debian::translation-check translation="a890161efc6ca4a77358f560b50009c8e1eb6e9c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Sandipan Roy a découvert deux vulnérabilités dans le programme unzip
d'InfoZIP, un extracteur de fichiers .zip, qui pouvaient avoir pour
conséquences un déni de service ou éventuellement l'exécution de code
arbitraire.</p>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
6.0-23+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unzip.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de unzip, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/unzip">\
https://security-tracker.debian.org/tracker/unzip</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3118.data"
# $Id: $

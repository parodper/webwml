<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Helmut Grohne discovered a flaw in Heimdal, an implementation of Kerberos 5 that
aims to be compatible with MIT Kerberos. The backports of fixes for
<a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a> accidentally inverted important memory comparisons in the
<code>arcfour-hmac-md5</code> and <code>rc4-hmac</code> integrity check handlers for gssapi,
resulting in incorrect validation of message integrity codes.</p>

<p>For Debian 10 buster, this problem has been fixed in version
7.5.0+dfsg-3+deb10u2.</p>

<p>We recommend that you upgrade your heimdal packages.</p>

<p>For the detailed security status of heimdal please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/heimdal">https://security-tracker.debian.org/tracker/heimdal</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3311.data"
# $Id: $

<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Joe Vennix discovered an authentication bypass vulnerability in dbus, an
asynchronous inter-process communication system. The implementation of
the DBUS_COOKIE_SHA1 authentication mechanism was susceptible to a
symbolic link attack. A local attacker could take advantage of this flaw
to bypass authentication and connect to a DBusServer with elevated
privileges.</p>

<p>The standard system and session dbus-daemons in their default
configuration are not affected by this vulnerability.</p>

<p>The vulnerability was addressed by upgrading dbus to a new upstream
version 1.10.28 which includes additional fixes.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 1.10.28-0+deb9u1.</p>

<p>We recommend that you upgrade your dbus packages.</p>

<p>For the detailed security status of dbus please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/dbus">https://security-tracker.debian.org/tracker/dbus</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4462.data"
# $Id: $

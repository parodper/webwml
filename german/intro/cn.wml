#use wml::debian::template title="Debian-Webseiten in verschiedenen Sprachen" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="80696988195221adfe32e0c037530145acd35b48"
# Translator: Philipp Frauenfelder <pfrauenf@debian.org>
# Changed-by: Martin Schulze <joey@debian.org>
# Updated: Holger Wansing <linux@wansing-online.de>, 2011, 2014, 2016.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020, 2023.

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">Inhalts-Navigation</a></li>
    <li><a href="#howtoset">Konfigurieren der Sprache im Web-Browser</a></li>
    <li><a href="#override">Überschreiben der Spracheinstellung des Browsers</a></li>
    <li><a href="#fix">Fehlerbehebung</a></li>
  </ul>
</div>

<h2><a id="intro">Inhalts-Navigation</a></h2>

<p>Ein Team von <a href="../devel/website/translating">Übersetzern</a>
   arbeitet daran, die Debian-Website in eine wachsende Anzahl von Sprachen
   zu übersetzen. Aber wie funktioniert die Umschaltung der Anzeigesprache
   im Web-Browser? Ein Standard namens
   <a href="$(HOME)/devel/website/content_negotiation">Inhaltsaushandlung</a>
   erlaubt es dem Benutzer, die bevorzugten Sprachen einzustellen, in denen er die
   Inhalte erhalten möchte. Die Version, die er schließlich zu sehen bekommt, wird
   zwischen dem Browser und dem Server ausgehandelt:
   der Browser schickt die Voreinstellungen zum Server, der aufgrund dieser
   Einstellungen und den vorhandenen Versionen entscheidet, welche Version des
   Dokuments ausgeliefert wird.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">Weitere Details beim W3C</a></button></p>

<p>Nicht alle Benutzer kennen den Standard zur Inhaltsaushandlung, daher gibt es
   Links am Fuß aller Debian-Webseiten, die zu Versionen der Seiten in anderen
   Sprachen führen. Beachten Sie, dass die Auswahl einer anderen Sprache über diese
   Links nur die Anzeige der aktuellen
   Seite betrifft. Dies ändert nicht die Standardsprache Ihres Browsers.
   Falls Sie anschließend auf eine andere Seite wechseln, wird diese wieder in der
   Standardsprache laut Browser-Einstellungen dargestellt.
</p>

<p>Um die Standardsprache Ihres Browsers zu ändern, haben Sie zwei Optionen:
</p>

<ul>
  <li><a href="#howtoset">Konfigurieren der Sprache im Web-Browser</a></li>
  <li><a href="#override">Überschreiben der Spracheinstellung des Browsers</a></li>
</ul>

<p>Um direkt zu den Konfigurationsbeispielen für einige beliebte Browser zu springen:
</p>

<toc-display />

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Die Standardsprache der Debian-Website
   ist Englisch. Daher ist es eine gute Idee, zusätzlich zu Ihren bevorzugten Sprachen noch
   Englisch (<code>en</code>) als letzten Eintrag in der Sprachliste einzutragen.
   Dies fungiert dann als eine Art Backup, falls die angeforderte Seite nicht in eine der
   von Ihnen bevorzugten Sprachen übersetzt ist.
</p>
</aside>

<h2><a id="howtoset">Konfigurieren der Sprache im Web-Browser</a></h2>

<p>Bevor wir beschreiben, wie Sie die Sprache in verschiedenen Browsern einstellen,
   zunächst einige grundlegende Hinweise. Punkt 1: Sie sollten alle Sprachen, die
   Sie sprechen, zu Ihrer Liste der bevorzugten Sprachen hinzufügen. Wenn Sie
   z.B. Deutsch als Muttersprache sprechen, sollten Sie <code>de</code> als erste
   Sprache einstellen, gefolgt von Englisch mit dem Sprachcode <code>en</code>.
</p>

<p>Als zweiten Punkt sollten Sie beachten, dass Sie in einigen Browsern nur
   Sprachcodes eintippen können, statt diese aus einer Liste auszuwählen.
   Dabei sollten Sie bedenken, dass eine Liste wie <code>de, en</code> noch
   nicht festlegt, welche Sprache aus der Liste bevorzugt wird. Stattdessen werden die angegebenen
   Werte als gleichrangige Optionen betrachtet, und der Server könnte entscheiden, die
   angegebene Reihenfolge zu ignorieren, und einfach irgendeine der Sprachen auswählen.
   Wenn Sie wirkliche Präferenzen angeben wollen, müssen Sie sogenannte <q>Qualitätswerte</q>
   (<q>quality values</q>) verwenden. Dabei handelt es sich um Gleitkommazahlen
   zwischen 0 und 1, wobei höhere Werte eine höhere Präferenz bedeuten. Im obigen
   Fall würden Sie also wahrscheinlich eine Einstellung wie die folgende bevorzugen:
</p>

<pre>
de; q=1.0, en; q=0.5
</pre>

<h3>Seien Sie vorsichtig mit Ländercodes</h3>

<p>Ein Webserver, der eine Anfrage für eine Seite mit der bevorzugten Spracheinstellung
   <code>en-GB, fr</code> erhält, wird <strong>nicht zwingend</strong> eine
   englische Version gegenüber der französischen bevorzugen. Er wird dies nur
   tun, wenn eine Seite mit der Spracherweiterung <code>en-gb</code> existiert.
   Andersherum funkitioniert es allerdings wohl: ein Server kann eine Seite mit
   der Spracherweiterung <code>en-us</code> ausliefern, wenn lediglich
   <code>en</code> in den Einstellungen des Browsers angegeben ist.
</p>

<p>Wir empfehlen daher, keine Ländererweiterungen wie <code>en-GB</code> oder
   <code>en-US</code> anzugeben, außer Sie haben wirklich gute Gründe dafür.
   Wenn Sie eine hinzufügen, stellen Sie sicher, dass Sie auch die Sprache
   ohne die Ländererweiterung hinzufügen: z.B. <code>en-GB, en, fr</code>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://httpd.apache.org/docs/current/content-negotiation.html">Näheres zum Thema Inhaltsaushandlung</a></button></p>

<h3>Anweisungen für verschiedene Web-Browser</h3>

<p>Wir haben eine Liste populärer Browser erstellt mit einigen Anweisungen,
   wie Sie bei diesen die bevorzugte Sprache für Web-Inhalte einstellen:
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
            Öffnen Sie oben rechts das Menü und klicken Sie auf <em>Einstellungen</em> -&gt; <em>Erweitert</em> -&gt; <em>Sprachen</em>. 
            Öffnen Sie das <em>Sprache</em>-Menü, um die Liste von Sprachen anzuzeigen. Klicken Sie auf die drei Punkte neben
            einem Eintrag, um die Reihenfolge zu ändern. Sie können auch eine Sprache hinzufügen, falls erforderlich.</li>
        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
            Das Ändern der Standardsprache über <em>Setup</em> -&gt; <em>Language</em> wird auch die angeforderte Spracheinstellung
            für Webseiten ändern. Sie können dieses Verhalten anpassen über eine Feineinstellung der <em>Accept-Language-Kopfzeile</em>
            unter <em>Setup</em> -&gt; <em>Options manager</em> -&gt; <em>Protocols</em> -&gt; <em>HTTP</em></li>
        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
            Öffnen Sie <em>Bearbeiten</em> aus dem Hauptmenü und wechseln Sie zur Lasche <em>Sprache</em>. Hier können Sie
            Sprachen hinzufügen, entfernen und sortieren.</li>
        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
            In der Menüzeile oben öffnen Sie <em>Einstellungen</em>. Scrollen Sie bei <em>Allgemein</em> nach unten bis <em>Sprache und 
            Erscheinungsbild</em> -&gt; <em>Sprache</em>. Klicken Sie den Knopf <em>Wählen</em>, um Ihre bevorzugte Sprache auszuwählen.
            Im gleichen Dialog können Sie auch Sprachen hinzufügen, entfernen und sortieren.</li>
        <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
            Gehen Sie zu <em>Preferences</em> -&gt; <em>Settings</em> -&gt; <em>Network</em>. Unter <em>Accept language</em> wird
            wahrscheinlich ein * angezeigt; dies ist die Standardeinstellung. Wenn Sie auf den Knopf <em>Locale</em> klicken,
            sollten Sie Ihre bevorzugte Sprache hinzufügen können. Falls nicht, können Sie sie manuell eingeben.</li>
        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
            <em>Edit</em> -&gt; <em>Preferences</em> -&gt; <em>Browser</em> -&gt; <em>Fonts, Languages</em></li>
        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
            <em>Bearbeiten</em> -&gt; <em>Einstellungen</em> -&gt; <em>Inhalt</em> -&gt; <em>Sprachen</em> -&gt; <em>Wählen</em></li>
        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
            Klicken Sie auf das <em>Tools</em>-Icon, wählen Sie <em>Internet options</em>, schalten Sie um auf die Lasche
            <em>General</em>, und klicken Sie auf den Knopf <em>Languages</em>. Klicken Sie auf <em>Set Language Preferences</em>,
            und im folgenden Dialog können Sie Ihre Sprachen hinzufügen, entfernen oder sortieren.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
            Editieren Sie die Datei <em>~/.kde/share/config/kio_httprc</em> und fügen Sie eine Zeile der
            folgenden Art ein:<br>
            <code>Languages=de;q=1.0, en;q=0.5</code></li>
        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
            Editieren Sie die Datei <em>~/.lynxrc</em> und fügen Sie eine Zeile der
            folgenden Art ein:<br>
            <code>preferred_language=de; q=1.0, en; q=0.5</code><br>
            Alternativ können Sie auch die Browser-Einstellungen öffnen, indem Sie [O] drücken. Scrollen Sie
            nach unten bis <em>Preferred language</em> und fügen Sie die Beispielzeile von oben ein.</li>
        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
            <em>Settings and more</em>  -&gt; <em>Settings</em> -&gt; <em>Languages</em> -&gt; <em>Add languages</em><br>
            Klicken Sie auf den Knopf mit den drei Punkten neben einem Spracheintrag, um weitere Optionen zu
            erhalten und die Reihenfolge zu ändern.</li>
        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
            <em>Settings</em> -&gt; <em>Browser</em> -&gt; <em>Languages</em> -&gt; <em>Preferred languages</em></li>
        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
            Safari nutzt die systemweiten Einstellungen von macOS und iOS; um also Ihre bevorzugte Sprache
            einzustellen, öffnen Sie <em>System Preferences</em> (macOS) oder <em>Settings</em> (iOS).</li>
        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
            Drücken Sie [O] um das <em>Option Setting Panel</em> zu öffnen, scrollen Sie nach unten bis
            <em>Network Settings</em> -&gt; <em>Accept-Language header</em>. Drücken Sie [Enter] um die Einstellungen
            zu ändern (z.B. <code>de; q=1.0, en; q=0.5</code>) und bestätigen Sie mit [Enter]. Scrollen Sie bis ganz
            nach unten, um die Einstellungen mit [OK] zu speichern.</li>
        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
            Gehen Sie zu <em>Settings</em> -&gt; <em>General</em> -&gt; <em>Language</em> -&gt; <em>Accepted Languages</em>,
            klicken Sie auf <em>Add Language</em> und wählen Sie eine der Sprachen aus dem Menü aus. Nutzen Sie
            die Pfeiltasten, um die Reihenfolge anzupassen.</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Es ist zwar immer besser, die bevorzugte Sprache in den Browser-Einstellungen
   einzustellen, aber es auch die Möglichkeit, diese Einstellung mit einem Cookie zu überschreiben.</p>
</aside>

<h2><a id="override">Überschreiben der Spracheinstellung des Browsers</a></h2>

<p>Falls Sie aus irgendeinem Grund die bevorzugte Sprache nicht in Browser-Einstellungen,
   Geräteeinstellungen oder im Betriebssystem konfigurieren können, besteht als letzte
   Chance noch die Möglichkeit, die Einstellungen mit einem Cookie zu überschreiben.
   Klicken Sie auf einen der Knöpfe unten, um die entsprechende Sprache an
   die Spitze der Liste Ihrer bevorzugten Sprachen zu setzen.
</p>

<p>Bitte beachten Sie, dass hierdurch ein
   <a href="https://en.wikipedia.org/wiki/HTTP_cookie">Cookie</a> gesetzt wird.
   Ihr Browser wird das Cookie selbsttätig löschen, wenn Sie diese Webseite für einen
   Monat nicht besuchen.
   Sie können das Cookie natürlich auch manuell in Ihrem Browser löschen, oder indem Sie
   den Knopf <q>Browser-Standardeinstellung</q> (<em>Browser default</em>) betätigen.
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>


<h2><a id="fix">Fehlerbehebung</a></h2>

<p>Manchmal wird die Debian-Website in der falschen Sprache angezeigt, trotz aller
   Bemühungen, die bevorzugte Sprache korrekt einzustellen.
   Unsere erste Empfehlung ist, den lokalen Cache (Zwischenspeicher) - sowohl
   auf der Festplatte wie auch im Arbeitsspeicher - über den Browser zu
   leeren, bevor Sie versuchen, die Seite erneut zu laden.
   Wenn Sie sich absolut sicher sind, dass Sie Ihren Browser
   <a href="#howtoset">korrekt konfiguriert</a> haben, könnte ein beschädigter oder
   falsch konfigurierter Cache das Problem sein. Dies wird mittlerweile ein immer
   größeres Problem, da immer mehr Internet-Provider Caching als Möglichkeit sehen,
   ihren Netzwerk-Traffic zu reduzieren. Lesen Sie den <a href="#cache">Abschnitt
   über Proxy-Server</a>, auch wenn Sie glauben, keinen zu benutzen.
</p>

<p>Neben allem anderen besteht auch immer die Möglichkeit, dass es ein Problem mit
   <a href="https://www.debian.org/">www.debian.org</a> gibt. Während nur
   eine Handvoll von Problemen, die in den letzten Jahren wegen
   falsch angzeigter Sprachen berichtet wurden, auf einen Fehler auf unserer Seite
   zurückzuführen war, ist dies natürlich trotzdem möglich. 
   Wir bitten Sie daher, zuerst Ihre Browser-Einstellungen sowie ein
   mögliches Caching-Problem genau zu untersuchen, bevor Sie
   <a href="../contact">uns ansprechen</a>. 
   Wenn <a href="https://www.debian.org/">https://www.debian.org/</a> funktioniert,
   jedoch einer der <a href="https://www.debian.org/mirror/list">Spiegel-Server</a>
   nicht, berichten Sie dies bitte, damit wir die Spiegel-Administratoren
   benachrichtigen können.
</p>

<h3><a name="cache">Potenzielle Probleme mit Proxy-Servern</a></h3>

<p>Proxy-Server sind im wesentlichen Web-Server, die keine eigenen Inhalte
   haben. Sie befinden sich in der Mitte zwischen dem Benutzer und den echten
   Web-Servern. Sie nehmen Ihre Anfrage für Webseiten entgegen und holen sich die
   Seite. Anschließend leiten sie die Seite an Sie weiter, machen aber gleichzeitig
   auch eine lokale, zwischengespeicherte Kopie für spätere Anfragen. Das kann den
   Netzwerk-Verkehr stark verringern, wenn mehrere Benutzer dieselbe Seite
   anfordern.
</p>

<p>Das ist meistens eine großartige Idee, sie schlägt jedoch fehl, wenn
   der Cache fehlerhaft ist. Insbesondere einige ältere Proxy-Server verstehen
   Inhaltsaushandlung nicht. Das resultiert darin, dass sie Seiten in einer
   Sprache zwischenspeichern und später ausliefern, auch wenn eine andere
   Sprache angefordert wurde. Die einzige Lösung besteht darin, die
   Caching-Software zu aktualisieren oder auszutauschen.
</p>

<p>Historisch gesehen benutzen Personen nur dann einen Proxy, wenn sie
   ihre Browser so konfiguriert haben, einen zu benutzen. Dies ist heute
   nicht mehr der Fall. Ihr ISP leitet möglicherweise alle Ihre
   HTTP-Anfragen durch einen transparenten Proxy. Wenn dieser Proxy
   Inhaltsaushandlung nicht vernünftig handhabt, könnten Benutzer die
   zwischengespeicherten Seiten in der falschen Sprache erhalten. Der einzige
   Weg, wie Sie das beheben können, ist, sich bei Ihrem ISP zu beschweren, damit
   dieser seine Software aktualisiert oder ersetzt.
</p>

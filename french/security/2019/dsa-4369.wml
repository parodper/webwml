#use wml::debian::translation-check translation="8a1c0e346cc4b60809eb2067ebcb114fe8cc027d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'hyperviseur Xen :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19961">CVE-2018-19961</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-19962">CVE-2018-19962</a>

<p>Paul Durrant a découvert qu'un traitement incorrect du TLB pourrait
avoir pour conséquence un déni de service, une augmentation de droits ou
des fuites d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19965">CVE-2018-19965</a>

<p>Matthew Daley a découvert qu'un traitement incorrect de l'instruction
INVPCID pourrait avoir pour conséquence un déni de service par des clients
de paravirtualisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19966">CVE-2018-19966</a>

<p>Une régression dans la correction pour traiter
<a href="https://security-tracker.debian.org/tracker/CVE-2017-15595">CVE-2017-15595</a>
pourrait avoir pour conséquence un déni de service, une augmentation de
droits ou des fuites d'informations par un client de paravirtualisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19967">CVE-2018-19967</a>

<p>Une erreur dans certains processeurs Intel pourrait avoir pour
conséquence un déni de service par une instance de client.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 4.8.5+shim4.10.2+xsa282-1+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xen, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4369.data"
# $Id: $

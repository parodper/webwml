#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Josef Gajdusek a découvert deux vulnérabilités dans gtk-vnc, un composant
graphique de visualisation VNC pour GTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5884">CVE-2017-5884</a>

<p>Correction de vérification de limites pour les encodages RRE, hextile et
copyrec. Ce bogue permettait à un serveur distant de provoquer un déni de
service par dépassement de tampon à l'aide d'un message soigneusement contrefait
contenant des sous-rectangles en dehors de la zone de dessin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5885">CVE-2017-5885</a>

<p>Validation correcte des index de plage de carte de couleurs. Ce bogue
permettait à un serveur distant de provoquer un déni de service par dépassement
de tampon à l'aide d'un message soigneusement contrefait avec des valeurs de
couleur hors plage.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.5.0-3.1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gtk-vnc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-831.data"
# $Id: $

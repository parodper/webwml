<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Three vulnerabilities have been fixed that could, under rare circumstances, lead to
remotely exploitable DoS vulnerabilities in software using exiv2 for meta-data
extraction.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11683">CVE-2017-11683</a>

    <p>Crash due to a reachable assertion on crafted input</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19716">CVE-2020-19716</a>

    <p>Buffer overflow when handling crafted meta-data of CRW images</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.25-4+deb10u3.</p>

<p>We recommend that you upgrade your exiv2 packages.</p>

<p>For the detailed security status of exiv2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/exiv2">https://security-tracker.debian.org/tracker/exiv2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3186.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that missing input validation in minidlna, a lightweight
DLNA/UPnP-AV server could result in the execution of arbitrary code. In
addition minidlna was susceptible to the <q>CallStranger</q> UPnP
vulnerability.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.1.6+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your minidlna packages.</p>

<p>For the detailed security status of minidlna please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/minidlna">https://security-tracker.debian.org/tracker/minidlna</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2489.data"
# $Id: $

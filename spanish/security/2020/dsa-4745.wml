#use wml::debian::translation-check translation="7fbf113ef094837f72d2bdb71154488accfe2afb"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el servidor de correo electrónico
Dovecot.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">CVE-2020-12100</a>

    <p>La recepción de correo con partes MIME profundamente anidadas da lugar a agotamiento
    de recursos cuando Dovecot intenta analizarlo sintácticamente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12673">CVE-2020-12673</a>

    <p>La implementación de NTLM en Dovecot no comprueba correctamente el tamaño del área de memoria
    de los mensajes, lo que da lugar a caídas al leer más allá del área asignada.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12674">CVE-2020-12674</a>

    <p>La implementación del mecanismo RPA en Dovecot acepta mensajes de longitud cero,
    lo que da lugar, más tarde, a caídas de aserción.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1:2.3.4.1-5+deb10u3.</p>

<p>Le recomendamos que actualice los paquetes de dovecot.</p>

<p>Para información detallada sobre el estado de seguridad de dovecot, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/dovecot">https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4745.data"

#use wml::debian::translation-check translation="9264c23e43e374bf590c29166dde81515943b562"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.12</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la duodécima actualización de su
distribución «antigua estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>



<h2>Endurecimiento de la comprobación de los algoritmos de firma de OpenSSL</h2>

<p>La actualización de OpenSSL proporcionada en esta versión incluye un
cambio para asegurar que el algoritmo de firma solicitado está
soportado en el nivel de seguridad activo.</p>

<p>Aunque eso no afectará a la mayoría de los casos de uso, podría dar lugar a
la emisión de mensajes de error si se solicita un algoritmo no
soportado. Por ejemplo, si se utilizan firmas RSA+SHA1 con el nivel
de seguridad 2, que es el nivel de seguridad por omisión.</p>

<p>En casos así, el nivel de seguridad tiene que ser rebajado
explícitamente, ya sea para operaciones individuales o de forma más global. Esto
puede requerir cambios en la configuración de las aplicaciones. Para
el propio OpenSSL, se puede rebajar el nivel de seguridad en operaciones individuales con una
opción de línea de órdenes como:</p>

<p>-cipher <q>ALL:@SECLEVEL=1</q></p>

<p>residiendo la configuración relevante a nivel de sistema en
/etc/ssl/openssl.cnf</p>


<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «antigua estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction apache-log4j1.2 "Resuelve problemas de seguridad [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307] eliminando el soporte de los módulos JMSSink, JDBCAppender, JMSAppender y Apache Chainsaw">
<correction apache-log4j2 "Corrige problema de ejecución de código remoto [CVE-2021-44832]">
<correction atftp "Corrige problema de fuga de información [CVE-2021-46671]">
<correction base-files "Actualizado para la versión 10.12">
<correction beads "Recompilado contra cimg actualizado para corregir varios desbordamientos de memoria dinámica («heap») [CVE-2020-25693]">
<correction btrbk "Corrige regresión en la actualización para CVE-2021-38173">
<correction cargo-mozilla "Nuevo paquete, retroadaptado de Debian 11, para ayudar a compilar nuevas versiones de rust">
<correction chrony "Permite leer el fichero de configuración de chronyd que genera timemaster(8)">
<correction cimg "Corrige problemas de desbordamiento de memoria dinámica («heap») [CVE-2020-25693]">
<correction clamav "Nueva versión «estable» del proyecto original; corrige problema de denegación de servicio [CVE-2022-20698]">
<correction cups "Corrige <q>un problema de validación de la entrada podría permitir que una aplicación maliciosa leyera memoria restringida</q> [CVE-2020-10001]">
<correction debian-installer "Recompilado contra oldstable-proposed-updates; actualiza la ABI del núcleo a la -20">
<correction debian-installer-netboot-images "Recompilado contra oldstable-proposed-updates">
<correction detox "Corrige el tratamiento de ficheros grandes en arquitecturas ARM">
<correction evolution-data-server "Corrige caída por respuesta del servidor mal construida [CVE-2020-16117]">
<correction flac "Corrige problema de lectura fuera de límites [CVE-2020-0499]">
<correction gerbv "Corrige problema de ejecución de código [CVE-2021-40391]">
<correction glibc "Importa varias correcciones de la rama «estable» del proyecto original; simplifica la comprobación de versiones del núcleo soportadas, puesto que los núcleos 2.x ya no lo están; soporta la instalación con núcleos cuyo número de versión es mayor de 255">
<correction gmp "Corrige problemas de desbordamiento de entero y de memoria [CVE-2021-43618]">
<correction graphicsmagick "Corrige problema de desbordamiento de memoria [CVE-2020-12672]">
<correction htmldoc "Corrige problema de lectura fuera de límites [CVE-2022-0534] y problemas de desbordamiento de memoria [CVE-2021-43579 CVE-2021-40985]">
<correction http-parser "Resuelve una rotura inadvertida de la ABI">
<correction icu "Corrige la utilidad <q>pkgdata</q>">
<correction intel-microcode "Actualiza el microcódigo incluido; mitiga algunos problemas de seguridad [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction jbig2dec "Corrige problema de desbordamiento de memoria [CVE-2020-12268]">
<correction jtharness "Nueva versión del proyecto original para soportar compilaciones de versiones de OpenJDK-11 más recientes">
<correction jtreg "Nueva versión del proyecto original para soportar compilaciones de versiones de OpenJDK-11 más recientes">
<correction lemonldap-ng "Corrige proceso de autorización en extensiones («plugins») de prueba de contraseñas [CVE-2021-20874]; añade «recomienda» gsfonts, corrigiendo captcha">
<correction leptonlib "Corrige problema de denegación de servicio [CVE-2020-36277] y problemas de lectura de memoria fuera de límites [CVE-2020-36278 CVE-2020-36279 CVE-2020-36280 CVE-2020-36281]">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libencode-perl "Corrige una fuga de memoria en Encode.xs">
<correction libetpan "Corrige problema de inyección de respuesta a STARTTLS [CVE-2020-15953]">
<correction libextractor "Corrige problema de lectura inválida [CVE-2019-15531]">
<correction libjackson-json-java "Corrige problemas de ejecución de código [CVE-2017-15095 CVE-2017-7525] y problemas de entidad externa XML [CVE-2019-10172]">
<correction libmodbus "Corrige problemas de lectura fuera de límites [CVE-2019-14462 CVE-2019-14463]">
<correction libpcap "Comprueba la longitud de la cabecera PHB antes de utilizarla para asignar memoria [CVE-2019-15165]">
<correction libsdl1.2 "Trata eventos de foco a la entrada correctamente; corrige problemas de desbordamiento de memoria [CVE-2019-13616 CVE-2019-7637] y problemas de lectura de memoria fuera de límites [CVE-2019-7572 CVE-2019-7573 CVE-2019-7574 CVE-2019-7575 CVE-2019-7576 CVE-2019-7577 CVE-2019-7578 CVE-2019-7635 CVE-2019-7636 CVE-2019-7638]">
<correction libxml2 "Corrige problema de «uso tras liberar» [CVE-2022-23308]">
<correction linux "Nueva versión «estable» del proyecto original; [rt] actualiza a la 4.19.233-rt105; incrementa la ABI a la 20">
<correction linux-latest "Actualiza a la ABI 4.19.0-20">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original; [rt] actualiza a la 4.19.233-rt105; incrementa la ABI a la 20">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original; [rt] actualiza a la 4.19.233-rt105; incrementa la ABI a la 20">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original; [rt] actualiza a la 4.19.233-rt105; incrementa la ABI a la 20">
<correction llvm-toolchain-11 "Nuevo paquete, retroadaptado de Debian 11, para ayudar a compilar nuevas versiones de rust">
<correction lxcfs "Corrige error en la información de uso del espacio de intercambio">
<correction mailman "Corrige problema de ejecución de scripts entre sitios («cross-site scripting») [CVE-2021-43331]; corrige <q>el moderador de una lista puede descifrar la contraseña del administrador, cifrada en un token CSRF</q> [CVE-2021-43332]; corrige potencial ataque CSRF contra un administrador de lista por parte de un miembro o moderador de la lista [CVE-2021-44227]; corrige regresiones en las correcciones de CVE-2021-42097 y de CVE-2021-44227">
<correction mariadb-10.3 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction node-getobject "Corrige problema de contaminación de prototipo [CVE-2020-28282]">
<correction opensc "Corrige problemas de acceso fuera de límites [CVE-2019-15945 CVE-2019-15946], caída debida a lectura de memoria desconocida [CVE-2019-19479], problema de «doble liberación» [CVE-2019-20792] y problemas de desbordamiento de memoria [CVE-2020-26570 CVE-2020-26571 CVE-2020-26572]">
<correction openscad "Corrige desbordamientos de memoria en el analizador sintáctico de STL [CVE-2020-28599 CVE-2020-28600]">
<correction openssl "Nueva versión del proyecto original">
<correction php-illuminate-database "Corrige problema de asociación de consulta («query binding») [CVE-2021-21263] y problema de inyección de SQL cuando se usa con Microsoft SQL Server">
<correction phpliteadmin "Corrige problema de ejecución de scripts entre sitios («cross-site scripting») [CVE-2021-46709]">
<correction plib "Corrige problema de desbordamiento de entero [CVE-2021-38714]">
<correction privoxy "Corrige fuga de memoria [CVE-2021-44540] y problema de ejecución de scripts entre sitios («cross-site scripting») [CVE-2021-44543]">
<correction publicsuffix "Actualiza los datos incluidos">
<correction python-virtualenv "Evita intentar la instalación de pkg_resources desde PyPI">
<correction raptor2 "Corrige problema de acceso a una matriz fuera de límites [CVE-2020-25713]">
<correction ros-ros-comm "Corrige problema de denegación de servicio [CVE-2021-37146]">
<correction rsyslog "Corrige problemas de desbordamiento de memoria dinámica («heap») [CVE-2019-17041 CVE-2019-17042]">
<correction ruby-httpclient "Usa el almacén de certificados del sistema">
<correction rust-cbindgen "Nueva versión «estable» del proyecto original para soportar compilaciones de versiones más recientes de firefox-esr y de thunderbird">
<correction rustc-mozilla "Nuevo paquete fuente para soportar la compilación de versiones más recientes de firefox-esr y de thunderbird">
<correction s390-dasd "Deja de pasar a dasdfmt la opción obsoleta («deprecated») -f">
<correction spip "Corrige problema de ejecución de scripts entre sitios («cross-site scripting»)">
<correction tzdata "Actualiza datos de Fiyi y de Palestina">
<correction vim "Corrige capacidad de ejecución de código en modo restringido [CVE-2019-20807], problemas de desbordamiento de memoria [CVE-2021-3770 CVE-2021-3778 CVE-2021-3875] y problema de «uso tras liberar» [CVE-2021-3796]; elimina parche incluido accidentalmente">
<correction wavpack "Corrige uso de valores no inicializados [CVE-2019-1010317 CVE-2019-1010319]">
<correction weechat "Corrige varios problemas de denegación de servicio [CVE-2020-8955 CVE-2020-9759 CVE-2020-9760 CVE-2021-40516]">
<correction wireshark "Corrige varios problemas de seguridad en disectores [CVE-2021-22207 CVE-2021-22235 CVE-2021-39921 CVE-2021-39922 CVE-2021-39923 CVE-2021-39924 CVE-2021-39928 CVE-2021-39929]">
<correction xterm "Corrige problema de desbordamiento de memoria [CVE-2022-24130]">
<correction zziplib "Corrige problema de denegación de servicio [CVE-2020-18442]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «antigua estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2019 4513 samba>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4989 strongswan>
<dsa 2021 4990 ffmpeg>
<dsa 2021 4991 mailman>
<dsa 2021 4993 php7.3>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4997 tiff>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5005 ruby-kaminari>
<dsa 2021 5006 postgresql-11>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5014 icu>
<dsa 2021 5015 samba>
<dsa 2021 5016 nss>
<dsa 2021 5018 python-babel>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5021 mediawiki>
<dsa 2021 5022 apache-log4j2>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5032 djvulibre>
<dsa 2022 5035 apache2>
<dsa 2022 5036 sphinxsearch>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5043 lxml>
<dsa 2022 5047 prosody>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5065 ipython>
<dsa 2022 5066 ruby2.5>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5078 zsh>
<dsa 2022 5081 redis>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5093 spip>
<dsa 2022 5096 linux-latest>
<dsa 2022 5096 linux-signed-amd64>
<dsa 2022 5096 linux-signed-arm64>
<dsa 2022 5096 linux-signed-i386>
<dsa 2022 5096 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5103 openssl>
<dsa 2022 5105 bind9>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction angular-maven-plugin "Ya no es útil">
<correction minify-maven-plugin "Ya no es útil">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «antigua estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «antigua estable» actual:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Actualizaciones propuestas a la distribución «antigua estable»:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Información sobre la distribución «antigua estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>



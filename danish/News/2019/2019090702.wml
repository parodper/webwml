#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7"
<define-tag pagetitle>Opdateret Debian 9: 9.10 udgivet</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den tiende opdatering af 
dets gamle stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer, sammen med 
nogle få rettelser af alvorlige problemer.  Sikkerhedsbulletiner er allerede 
udgivet separat og der vil blive refereret til dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Online-opdatering til denne revision gøres normalt ved at lade 
pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den gamle stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction base-files 					"Opdaterer til denne punktopdatering; tilføjer VERSION_CODENAME til os-release">
<correction basez 					"Dekoder på korrekt vis base64url-enkodede strenge">
<correction biomaj-watcher 				"Retter opgraderinger fra jessie til stretch">
<correction c-icap-modules 				"Tilføjer understøttelse af clamav 0.101.1">
<correction chaosreader 				"Tilføjer afhængighed af libnet-dns-perl">
<correction clamav 					"Ny stabil opstrømsudgave: tilføjer scanningstidsbegrænsning til afhjælpning af zip-bomber [CVE-2019-12625]; retter skrivning udenfor grænserne i NSIS' bzip2-bibliotek [CVE-2019-12900]">
<correction corekeeper 					"Anvend ikke globalt skrivbar /var/crash med dumper-skriptet; håndt gamle versioner af Linux-kernen på en mere sikker måde; forkort ikke core-navne ved udførbare filer med mellemrum">
<correction cups 					"Retter adskillige sikkerheds-/blotlæggelseproblemer - SNMP-bufferoverløb [CVE-2019-8696 CVE-2019-8675], IPP-bufferoverløb, lammelsesangreb og problemer med hukommelsesafsløring i scheduleren">
<correction dansguardian 				"Tilføjer understøttelse af clamav 0.101">
<correction dar 					"Genopbygger for at opdatere <q>built-using</q>-pakker">
<correction debian-archive-keyring 			"Tilføjer buster-nøgler; fjerner wheezy-nøgler">
<correction fence-agents 				"Retter lammelsesangrebsproblem [CVE-2019-10153]">
<correction fig2dev 					"Segfault ikke ved cirkel-/halvcirkel-pilehoveder med en forstørrelse på mere end 42 [CVE-2019-14275]">
<correction fribidi 					"Retter højre til venstre-uddata i debian-installers teksttilstand">
<correction fusiondirectory 				"Strengere kontroller af LDAP-opslag; tilføjer manglende afhængighed af php-xml">
<correction gettext 					"Får xgettext() til ikke længere at gå ned når den køres med valgmuligheden --its=FILE">
<correction glib2.0 					"Opretter mappe og fil med restriktive rettigheder når der anvendes GKeyfileSettingsBackend [CVE-2019-13012]; undgår buferlæsningsoverløb når der formatteres fejlmeddelelser for ugyldigt UTF-8 i GMarkup [CVE-2018-16429]; undgår NULL-dereference når der fortolkes ugyldig GMarkup med et misdannet lukke-tag som ikke har et korresponderende åbnings-tag [CVE-2018-16429]">
<correction gocode 					"gocode-auto-complete-el: Indfører præ-afhængighed af auto-complete-el versionieret til at rette opgraderinger fra jessie til stretch">
<correction groonga 					"Afhjælper rettighedsforøgelse ved at ændre logfilers ejer og gruppe med valgtmuligheden <q>su</q>">
<correction grub2 					"Retter Xen UEFI-understøttelsen">
<correction gsoap 					"Retter problem med lammelsesangreb hvis en serverapplikation er opbygget med flaget -DWITH_COOKIES [CVE-2019-7659]; retter problem med DIME-protokolmodtager og misdannede DIME-headere">
<correction gthumb 					"Retter dobbelt frigivelse-fejl [CVE-2018-18718]">
<correction havp 					"Tilføjer understøttelse af clamav 0.101.1">
<correction icu 					"Retter segfault i kommandoen pkgdata">
<correction koji 					"Retter SQL-indsprøjtningsproblem [CVE-2018-1002161]; validerer på korrekt vis SCM-stier [CVE-2017-1002153]">
<correction lemonldap-ng 				"Retter regression i autentifikation på tværs af domæner; retter sårbarhed i ekstern XML-entitet">
<correction libcaca 					"Retter heltalsoverløbsproblemer [CVE-2018-20545 CVE-2018-20546 CVE-2018-20547 CVE-2018-20548 CVE-2018-20549]">
<correction libclamunrar 				"Ny stabil opstrømsudgave">
<correction libconvert-units-perl 			"Genopbygning uden ændring med rettet versionsnummer">
<correction libdatetime-timezone-perl 			"Opdateer medfølgende data">
<correction libebml 					"Anvender opstrømsrettelser af heapbaserede buferoverlæsninger">
<correction libevent-rpc-perl 				"Retter opbygningsfejl på grund af udløbet SSL-testcertifikater">
<correction libgd2 					"Retter uinitialiseret læsning i gdImageCreateFromXbm [CVE-2019-11038]">
<correction libgovirt 					"Gen-genererer testcertifikater med udløbsdato langt ude i fremtiden for at undgå testfejl">
<correction librecad 					"Retter lammelsesangreb gennem fabrikeret fil [CVE-2018-19105]">
<correction libsdl2-image 				"Retter adskillige sikkerhedsproblemer">
<correction libthrift-java 				"Retter omgåelse af SASL-forhandling [CVE-2018-1320]">
<correction libtk-img 					"Holder op med at benytte interne kopier af JPEG-, Zlib- og PixarLog-codecs, retter nedbrud">
<correction libu2f-host 				"Retter stakhukommelseslækage [CVE-2019-9578]">
<correction libxslt 					"Retter omgåelse af sikkerhedsframework [CVE-2019-11068]; retter uinitialiseret læsning af xsl:number-token [CVE-2019-13117]; retter uinitialiseret læsning med UTF-8-grupperingstegn [CVE-2019-13118]">
<correction linux 					"Ny opstrømsversion med ABI-bump; sikkerhedsrettelser [CVE-2015-8553 CVE-2017-5967 CVE-2018-20509 CVE-2018-20510 CVE-2018-20836 CVE-2018-5995 CVE-2019-11487 CVE-2019-3882]">
<correction linux-latest 				"Opdaterer til 4.9.0-11-kerne-ABI">
<correction liquidsoap 					"Retter kompilering med Ocaml 4.02">
<correction llvm-toolchain-7 				"Ny pakke til understøttelse af opbygning af nye versioner af Firefox">
<correction mariadb-10.1 				"Ny stabil opstrømsudgave; sikkerhedsrettelser [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2805 CVE-2019-2627 CVE-2019-2614]">
<correction minissdpd 					"Forhindrer en anvendelse efter frigivelse-sårbarhed der gjorde det muligt for en fjernangriber at få en proces til at gå ned [CVE-2019-12106]">
<correction miniupnpd 					"Retter lammelsesangrebsproblemer [CVE-2019-12108 CVE-2019-12109 CVE-2019-12110]; retter informationslækage [CVE-2019-12107]">
<correction mitmproxy 					"Sortliser tests som kræver internetadgang; forhindrer indsættelse af uønskede versionerede afhængigheder på den øvre grænse">
<correction monkeysphere 				"Retter opbygningsfejl ved at opdatere testene til at tage højde for den opdaterede GnuPG i stretch nu har anderledes uddata">
<correction nasm-mozilla 				"Ny pakke til understøttelse af opbygning af nye versioner af Firefox">
<correction ncbi-tools6 				"Genpakker uden ikke-frie data/UniVec.*">
<correction node-growl 					"Fornuftighedskontrollerer inddata før de overføres til exec">
<correction node-ws 					"Begrænser uploadstørrelse [CVE-2016-10542]">
<correction open-vm-tools 				"Retter muligt sikkerhedsproblem med rettighederne til den mellemliggende forberedelsesmappe- og sti">
<correction openldap 					"Begrænser rootDN-proxyauthz til dens egne databaser [CVE-2019-13057]; håndhæver sasl_ssf's ACL-statement ved hver forbindelse [CVE-2019-13565]; retter slapo-rwm til ikke at frigive oprindeligt filter når det genskrevne filter er ugyldigt">
<correction openssh 					"Retter deadlock i nøglematching">
<correction passwordsafe 				"Installer ikke lokaltilpasningsfiler i en ekstra undermappe">
<correction pound 					"Retter forespørgselssmugling gennem fabrikerede headere [CVE-2016-10711]">
<correction prelink 					"Genopbygget for at opdatere <q>built-using</q>-pakke">
<correction python-clamav 				"Tilføjer understøttelse af clamav 0.101.1">
<correction reportbug 					"Opdaterer udgivelsesnavne, efter udgivelsen af buster">
<correction resiprocate 				"Løser et installeringsproblem med libssl-dev og --install-recommends">
<correction sash 					"Genopbygger for at opdatere <q>built-using</q>-pakker">
<correction sdl-image1.2 				"Retter bufferoverløb [CVE-2018-3977 CVE-2019-5058 CVE-2019-5052], tilgang udenfor grænserne [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction signing-party 				"Retter usikkert shell-kald som muliggjorde shell-indsprøjtning gennem en brugerid [CVE-2019-11627]">
<correction slurm-llnl 					"Retter potentielt heapoverløb på 32 bit-systemer [CVE-2019-6438]">
<correction sox 					"Retter flere sikkerhedsproblemer [CVE-2019-8354 CVE-2019-8355 CVE-2019-8356 CVE-2019-8357 927906 CVE-2019-1010004 CVE-2017-18189 881121 CVE-2017-15642 882144 CVE-2017-15372 878808 CVE-2017-15371 878809 CVE-2017-15370 878810 CVE-2017-11359 CVE-2017-11358 CVE-2017-11332">
<correction systemd 					"Stop ikke ndisc-klienten i tilfælde af opsætningsfejl">
<correction t-digest 					"Genopbygning uden ændringer for at undgå at genbrug af pre-epoch version 3.0-1">
<correction tenshi 					"Retter PID-filproblemer som gjorde det muligt for lokale brugere at dræbe vilkårlige processer [CVE-2017-11746]">
<correction tzdata 					"Ny opstrømsudgave">
<correction unzip 					"Retter ukorrekt fortolkning af 64 bit-værdier i fileio.c; retter problemer med zip-bombe [CVE-2019-13232]">
<correction usbutils 					"Opdaterer USB-id-liste">
<correction xymon 					"Retter flere sikkerhedsproblemer (kun server) [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubico-piv-tool 				"Retter sikkerhedsproblemer [CVE-2018-14779 CVE-2018-14780]">
<correction z3 						"Opsæt ikke SONAME hørende til libz3java.so til libz3.so.4">
<correction zfs-auto-snapshot 				"Får cronjobs til at afslutte i stilhed efter fjernelse af pakke">
<correction zsh 					"Genopbygger for at opdatere <q>built-using</q>-pakker">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den gamle stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2019 4435 libpng1.6>
<dsa 2019 4436 imagemagick>
<dsa 2019 4437 gst-plugins-base1.0>
<dsa 2019 4438 atftp>
<dsa 2019 4439 postgresql-9.6>
<dsa 2019 4440 bind9>
<dsa 2019 4441 symfony>
<dsa 2019 4442 cups-filters>
<dsa 2019 4442 ghostscript>
<dsa 2019 4443 samba>
<dsa 2019 4444 linux>
<dsa 2019 4445 drupal7>
<dsa 2019 4446 lemonldap-ng>
<dsa 2019 4447 intel-microcode>
<dsa 2019 4448 firefox-esr>
<dsa 2019 4449 ffmpeg>
<dsa 2019 4450 wpa>
<dsa 2019 4451 thunderbird>
<dsa 2019 4452 jackson-databind>
<dsa 2019 4453 openjdk-8>
<dsa 2019 4454 qemu>
<dsa 2019 4455 heimdal>
<dsa 2019 4456 exim4>
<dsa 2019 4457 evolution>
<dsa 2019 4458 cyrus-imapd>
<dsa 2019 4459 vlc>
<dsa 2019 4460 mediawiki>
<dsa 2019 4461 zookeeper>
<dsa 2019 4462 dbus>
<dsa 2019 4463 znc>
<dsa 2019 4464 thunderbird>
<dsa 2019 4465 linux>
<dsa 2019 4466 firefox-esr>
<dsa 2019 4467 vim>
<dsa 2019 4468 php-horde-form>
<dsa 2019 4469 libvirt>
<dsa 2019 4470 pdns>
<dsa 2019 4471 thunderbird>
<dsa 2019 4472 expat>
<dsa 2019 4473 rdesktop>
<dsa 2019 4475 openssl>
<dsa 2019 4475 openssl1.0>
<dsa 2019 4476 python-django>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4485 openjdk-8>
<dsa 2019 4487 neovim>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4492 postgresql-9.6>
<dsa 2019 4494 kconfig>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4506 qemu>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>				<th>Årsag</th></tr>
<correction pump 				"Vedligeholdes ikke; sikkerhedsproblemer">
<correction teeworlds 				"Sikkerhedsproblemer; ikke kompatibel med nuværende servere">
</table>


<h2>Debian Installer</h2>

Installeringsprogrammet er opdateret for at medtage rettelser indført i oldstable, 
i denne punktopdatering.


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle gamle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Foreslåede opdateringer til den gamle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Oplysninger om den gamle stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>

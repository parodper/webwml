#use wml::debian::translation-check translation="bf68ee71fed8c090db6b781966f9ab368e37015f" maintainer="Jean-Pierre Giraud"
<define-tag description>Fin de vie de la prise en charge à long terme de Debian 9</define-tag>
<define-tag moreinfo></p>
<p>
L'équipe du suivi à long terme (LTS) de Debian annonce par ce message
que la prise en charge de Debian 9 <q>Stretch</q> a atteint sa fin de vie
le 1er juillet 2022, cinq ans après sa publication initiale le 17 juin 2017.
</p>
<p>
Debian ne fournira plus désormais de mises à jour de sécurité pour
Debian 9. Un sous-ensemble de paquets de <q>Stretch</q> sera pris en charge
par des intervenants extérieurs. Vous trouverez des informations détaillées
sur la page <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>.
</p>
<p>
L'équipe LTS préparera la transition vers Debian 10 <q>Buster</q>, qui est
la distribution <q>oldstable</q> actuelle. L'équipe LTS prendra le relais
de l'équipe en charge de la sécurité pour le suivi durant le mois d'août,
tandis que la dernière mise à jour de <q>Buster</q> sera publiée ce même
mois.
</p>
<p>
Debian 10 recevra aussi un suivi à long terme de cinq ans à compter de sa
publication initiale et qui prendra fin le 30 juin 2024. Les architectures
prises en charges seront annoncées ultérieurement.
</p>
<p>
Pour plus d'informations sur l'utilisation de <q>Buster</q> LTS et la
mise à niveau à partir de <q>Stretch</q> LTS, veuillez vous référer à la
page du wiki <a href="https://wiki.debian.org/fr/LTS/Using">LTS/Using</a>.
</p>
<p>
Debian et l'équipe LTS aimeraient remercier tous ceux, utilisateurs
contributeurs, développeurs et parrains, qui rendent possible le
prolongement de la vie des anciennes distributions <q>stable</q> et qui ont
fait de LTS un succès.
</p>
<p>
Si vous dépendez de Debian LTS, vous pouvez envisager de
<a href="https://wiki.debian.org/fr/LTS/Development">rejoindre l'équipe</a>,
en fournissant des correctifs, en réalisant des tests ou en
<a href="https://wiki.debian.org/fr/LTS/Funding">finançant l'initiative</a>.
</p>
<p>
Vous trouverez plus d'informations sur le suivi à long terme (LTS) de
Debian sur la <a href="https://wiki.debian.org/LTS/">page LTS du wiki</a>.
</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3067.data"
# $Id: $

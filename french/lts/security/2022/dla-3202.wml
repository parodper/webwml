#use wml::debian::translation-check translation="35eab6086cb0e0ec903085461a49521116b10420" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Trois problèmes ont été découverts dans libarchive, une bibliothèque
d’archive multiformat et de compression.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19221">CVE-2019-19221</a>

<p>Lecture hors limites due à un appel incorrect à mbrtowc ou mbtowc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23177">CVE-2021-23177</a>

<p>Extraction de lien symbolique avec ACL modifiant l’ACL de la cible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31566">CVE-2021-31566</a>

<p>Lien symbolique incorrectement suivi lors du changement de modes, de datages,
d’ACL et de drapeaux d’un fichier lors de l’extraction d’une archive.</p>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 3.3.3-4+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libarchive.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libarchive,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libarchive">\
https://security-tracker.debian.org/tracker/libarchive</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3202.data"
# $Id: $

#use wml::debian::template title="Traduction des pages de manuel non spécifiques à Debian"
#use wml::debian::toc

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h3>%body</h3>
</define-tag>

<toc-display/>

<toc-add-entry name="quoi">De quoi s'agit-il&nbsp;?</toc-add-entry>

<p>
Il s'agit des pages de manuel non spécifiques à Debian et dont
les traductions ne peuvent pas être incluses dans le paquet d'origine.
Celles-ci font donc partie du paquet manpages-fr.</p>
<p>
Remarque : ces traductions sont utilisées par d’autres distributions (Arch Linux,
Fedora, Mageia, openSUSE).
</p>

<toc-add-entry name="responsable">Qui en est responsable&nbsp;?</toc-add-entry>
<p>
L’équipe debian-l10n est responsable de la publication des traductions des pages
de manuel.
</p>

<toc-add-entry name="a-traduire">Que faut-il traduire&nbsp;?</toc-add-entry>
<p>
Les statistiques sur la traduction des pages de manuel sont disponibles dans la
<a href="https://manpages-l10n-team.pages.debian.net/manpages-l10n/debian-unstable-fr.html">\
Liste des fichiers dont la traduction n'est pas complète</a>.
</p>
<p>
Le suivi des traductions est aussi disponible dans
l’<a href=https://www.debian.org/international/l10n/po/fr>État des fichiers PO
pour la langue française</a>.
</p>

<toc-add-entry name="fichiers">Où récupérer les fichiers à
traduire&nbsp;?</toc-add-entry>
<p>
Les fichiers sont disponibles dans le
<a href="https://salsa.debian.org/manpages-l10n-team/manpages-l10n/-/tree/master/po/fr/">\
dépôt git</a>.
</p>

<toc-add-entry name="format">Quel est le format des fichiers à
traduire&nbsp;?</toc-add-entry>

<p>Il s'agit du format PO. Nous vous conseillons de lire la
<a href="https://www.gnu.org/software/gettext/manual/html_node/gettext_toc.html">\
documentation de Gettext</a> ainsi que le <em>
<a href="http://l10n.kde.org/docs/translation-howto/">KDE Translation
HOWTO</a></em> pour bien le comprendre.</p>

<toc-add-entry name="comment">Comment traduire&nbsp;?</toc-add-entry>
<p>Il faut copier le fichier récupéré sous le nom <tt>fr.po</tt>, puis l'éditer
et le traduire, de préférence avec un outil dédié comme Lokalize ou poedit.</p>
<p>
À la fin de votre traduction, il faut remplir les champs suivants&nbsp;:</p>
<ul>
<li>Project-Id-Version: nom du paquet et version au moment de la
traduction</li>
<li>Last-Translator: le plus souvent votre outil mettra lui-même
<strong>vos</strong> prénom, nom et adresse électronique</li>
<li>Language-Team: "French &lt;debian-l10n-french@lists.debian.org&gt;\n"</li>
</ul>

<toc-add-entry name="coordination">Procédure de coordination</toc-add-entry>

<p>Le sous-projet concernant les pages de manuel non spécifiques à
Debian est désigné par <tt>man</tt>.
Le protocole à suivre est le suivant (référez vous à la page sur le
<a href="format#etat">format des messages</a> pour des explications plus
précises) :</p>
<dl>
<dt>
  <kbd>[ITT] man://<var>paquet</var>/fr.po</kbd>
</dt>
<dd>
Vous avez décidé de traduire une page de manuel et vous l'annoncez à la liste.
</dd>
<dt>
  <kbd>[RFR] man://<var>paquet</var>/fr.po</kbd>
</dt>
<dd>
Vous avez traduit ce document et vous demandez des relectures.
</dd>
<dt>
  <kbd>[LCFC] man://<var>paquet</var>/fr.po</kbd>
</dt>
<dd>
Vous avez intégré les relectures et vous pensez que cette traduction est
terminée.
</dd>
<dt>
  <kbd>[DONE] man://<var>paquet</var>/fr.po</kbd>
</dt>
<dd>
La traduction est considérée comme terminée. L'un des responsables récupérera
le fichier final que vous aurez pris soin d'attacher à votre message [done]
et l'intégrera dans le dépôt git du paquet manpages-fr. La page de manuel sera
disponible dès la prochaine mise à jour de ce paquet.
</dd>
</dl>

<toc-add-entry name="publier">Quand et comment publier votre
traduction&nbsp;?</toc-add-entry>
<p>La publication de votre traduction se fait en l'envoyant dans le
message [DONE]. Les responsables se chargeront du reste.</p>

<toc-add-entry name="maj">Comment mettre à jour votre
traduction&nbsp;?</toc-add-entry>
<p>Signalez à la liste que vous vous chargez de cette mise à jour en le
signalant par un <a href="format#etat"><tt>ITT</tt></a> et reprenez le protocole
ci-dessus.</p>

<toc-add-entry name="outils">Outils d’aide à la traduction</toc-add-entry>
<p>
Grégoire Scano a développé des
<a href=https://salsa.debian.org/l10n-fr-team/l10n-french/>outils</a> pour
aider, entre autres, à récupérer et mettre à jour les pages de manuel, puis
visualiser la construction des pages.
</p>

<p> Alban Vidal a développé un
<a href=https://salsa.debian.org/albanvidal-guest/po-check-translate>outil</a>
de vérification des traductions.
</p>

<p>Des outils po4a* permettent de visualiser ou construire les pages de manuel.
</p>

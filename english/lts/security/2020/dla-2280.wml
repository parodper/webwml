<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in Python, an interactive
high-level object-oriented language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20406">CVE-2018-20406</a>

    <p>Modules/_pickle.c has an integer overflow via a large LONG_BINPUT
    value that is mishandled during a <q>resize to twice the size</q>
    attempt. This issue might cause memory exhaustion, but is only
    relevant if the pickle format is used for serializing tens or
    hundreds of gigabytes of data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20852">CVE-2018-20852</a>

    <p>http.cookiejar.DefaultPolicy.domain_return_ok in
    Lib/http/cookiejar.py does not correctly validate the domain: it
    can be tricked into sending existing cookies to the wrong
    server. An attacker may abuse this flaw by using a server with a
    hostname that has another valid hostname as a suffix (e.g.,
    pythonicexample.com to steal cookies for example.com). When a
    program uses http.cookiejar.DefaultPolicy and tries to do an HTTP
    connection to an attacker-controlled server, existing cookies can
    be leaked to the attacker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5010">CVE-2019-5010</a>

    <p>An exploitable denial-of-service vulnerability exists in the X509
    certificate parser. A specially crafted X509 certificate can cause
    a NULL pointer dereference, resulting in a denial of service. An
    attacker can initiate or accept TLS connections using crafted
    certificates to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>

    <p>Improper Handling of Unicode Encoding (with an incorrect netloc)
    during NFKC normalization. The impact is: Information disclosure
    (credentials, cookies, etc. that are cached against a given
    hostname). The components are: urllib.parse.urlsplit,
    urllib.parse.urlparse. The attack vector is: A specially crafted
    URL could be incorrectly parsed to locate cookies or
    authentication data and send that information to a different host
    than when parsed correctly.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>

    <p>An issue was discovered in urllib2. CRLF injection is possible if
    the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically
    in the query string after a ? character) followed by an HTTP
    header or a Redis command.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a>

    <p>An issue was discovered in urllib2. CRLF injection is possible if
    the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically
    in the path component of a URL that lacks a ? character) followed
    by an HTTP header or a Redis command. This is similar to the
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a> query string issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9948">CVE-2019-9948</a>

    <p>urllib supports the local_file: scheme, which makes it easier for
    remote attackers to bypass protection mechanisms that blacklist
    file: URIs, as demonstrated by triggering a
    urllib.urlopen('local_file:///etc/passwd') call.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10160">CVE-2019-10160</a>

    <p>A security regression was discovered in python, which still allows
    an attacker to exploit <a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a> by abusing the user and
    password parts of a URL. When an application parses user-supplied
    URLs to store cookies, authentication credentials, or other kind
    of information, it is possible for an attacker to provide
    specially crafted URLs to make the application locate host-related
    information (e.g. cookies, authentication data) and send them to a
    different host than where it should, unlike if the URLs had been
    correctly parsed. The result of an attack may vary based on the
    application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16056">CVE-2019-16056</a>

    <p>The email module wrongly parses email addresses that contain
    multiple @ characters. An application that uses the email module
    and implements some kind of checks on the From/To headers of a
    message could be tricked into accepting an email address that
    should be denied. An attack may be the same as in <a href="https://security-tracker.debian.org/tracker/CVE-2019-11340">CVE-2019-11340</a>;
    however, this CVE applies to Python more generally.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16935">CVE-2019-16935</a>

    <p>The documentation XML-RPC server has XSS via the server_title
    field. This occurs in Lib/xmlrpc/server.py. If set_server_title is
    called with untrusted input, arbitrary JavaScript can be delivered
    to clients that visit the http URL for this server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18348">CVE-2019-18348</a>

    <p>An issue was discovered in urllib2. CRLF injection is possible if
    the attacker controls a url parameter, as demonstrated by the
    first argument to urllib.request.urlopen with \r\n (specifically
    in the host component of a URL) followed by an HTTP header. This
    is similar to the <a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a> query string issue and the
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a> path string issue</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8492">CVE-2020-8492</a>

    <p>Python allows an HTTP server to conduct Regular Expression Denial
    of Service (ReDoS) attacks against a client because of
    urllib.request.AbstractBasicAuthHandler catastrophic backtracking.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14422">CVE-2020-14422</a>

    <p>Lib/ipaddress.py improperly computes hash values in the
    IPv4Interface and IPv6Interface classes, which might allow a
    remote attacker to cause a denial of service if an application is
    affected by the performance of a dictionary containing
    IPv4Interface or IPv6Interface objects, and this attacker can
    cause many dictionary entries to be created.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.5.3-1+deb9u2.</p>

<p>We recommend that you upgrade your python3.5 packages.</p>

<p>For the detailed security status of python3.5 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python3.5">https://security-tracker.debian.org/tracker/python3.5</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2280.data"
# $Id: $

#use wml::debian::translation-check translation="24c857ab9e9769817510eaee4ab4860fd27c3a19"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Joe Vennix descubrió una vulnerabilidad de elusión de autenticación en dbus, un
sistema de comunicación asíncrona entre procesos. La implementación del
mecanismo de autenticación DBUS_COOKIE_SHA1 era vulnerable a un
ataque de enlace simbólico. Un atacante local podría aprovechar este defecto
para sortear la autenticación y conectarse a un DBusServer con elevación de
privilegios.</p>

<p>A los demonios dbus estándar de sistema y de sesión en su configuración
por omisión no les afecta esta vulnerabilidad.</p>

<p>La vulnerabilidad se abordó mediante la actualización de dbus a una nueva versión
del proyecto original, la 1.10.28, que incluye también otras correcciones.</p>

<p>Para la distribución «estable» (stretch), este problema se ha corregido en
la versión 1.10.28-0+deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de dbus.</p>

<p>Para información detallada sobre el estado de seguridad de dbus, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/dbus">https://security-tracker.debian.org/tracker/dbus</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4462.data"

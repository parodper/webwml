<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential resource exhaustion attack in
modsecurity-apache, an Apache module which inspects HTTP requests with the aim
of preventing typical web application attacks such as XSS and SQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42717">CVE-2021-42717</a>

    <p>ModSecurity 3.x through 3.0.5 mishandles excessively nested JSON
    objects. Crafted JSON objects with nesting tens-of-thousands deep could
    result in the web server being unable to service legitimate requests. Even
    a moderately large (e.g., 300KB) HTTP request can occupy one of the limited
    NGINX worker processes for minutes and consume almost all of the available
    CPU on the machine. Modsecurity 2 is similarly vulnerable: the affected
    versions include 2.8.0 through 2.9.4.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, this problem has been fixed in version
2.9.1-2+deb9u1.</p>

<p>We recommend that you upgrade your modsecurity-apache packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3031.data"
# $Id: $

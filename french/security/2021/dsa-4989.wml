#use wml::debian::translation-check translation="21199125ec27fcc04d1d656a9f0bf8050249091a" maintainer=""
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Des chercheurs de l'Agence nationale de la sécurité des USA (NSA) ont
identifié deux vulnérabilités de déni de services dans strongSwan, une
suite IKE/IPsec.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41990">CVE-2021-41990</a>

<p>Les signatures RSASSA-PSS dont les paramètres définissent une très
grande longueur de « sel » peuvent déclencher un dépassement d'entier qui
peut conduire à une erreur de segmentation.</p>

<p>La génération d'une signature qui contourne la vérification du
remplissage pour déclencher le plantage requiert l'accès à la clé privée
qui a signé le certificat. Néanmoins, le certificat n'a pas besoin d'être
fiable. Parce que les greffons gmp et openssl vérifient l'un et l'autre si
les certificats analysés sont auto-signés (et si la signature est valable),
cela peut par exemple être déclenché par un certificat d'autorité de
certification (CA) auto-signé sans rapport envoyé par un initiateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41991">CVE-2021-41991</a>

<p>Une fois que le cache de certificats en mémoire est plein, il essaye de
remplacer de façon aléatoire les entrées les moins utilisées. Selon la
valeur aléatoire générée, cela pourrait conduire à un dépassement d'entier
qui a pour conséquence un double déréférencement et un appel mémoire hors
limites qui mène très probablement à une erreur de segmentation.</p>

<p>L'exécution de code distant ne peut être totalement écartée, mais les
attaquants n'ont pas le contrôle sur la mémoire déréférencée, aussi elle
semble peu probable à ce stade.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 5.7.2-1+deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.9.1-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets strongswan.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de strongswan, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4989.data"
# $Id: $

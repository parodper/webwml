#use wml::debian::translation-check translation="6221e5601405ae9950ad42b0799d3bd4c92f5741" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10196">CVE-2018-10196</a>

<p>Une vulnérabilité de déréférencement de pointeur NULL dans la fonction
rebuild_vlists dans lib/dotgen/conc.c de la bibliothèque dotgen permet à des
attaquants distants de provoquer un déni de service (plantage d'application)
à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-18032">CVE-2020-18032</a>

<p>Un dépassement de tampon a été découvert dans Graphviz qui pourrait
éventuellement aboutir à l'exécution de code arbitraire lors du traitement d’un
fichier mal formé.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.38.0-17+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphviz.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de graphviz, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/graphviz">\
https://security-tracker.debian.org/tracker/graphviz</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2659.data"
# $Id: $

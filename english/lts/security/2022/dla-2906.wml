<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two vulnerabilities in Django, a popular
Python-based web development framework:</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22818">CVE-2022-22818</a>

    <p>Possible XSS via {% debug %} template tag.</p>
    
    <p>The {% debug %} template tag didn't properly encode the current context,
    posing an XSS attack vector.</p>

    <p>In order to avoid this vulnerability, {% debug %} no longer outputs
    information when the DEBUG setting is False, and it ensures all context
    variables are correctly escaped when the DEBUG setting is True.</p>
</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23833">CVE-2022-23833</a>

    <p>Denial-of-service possibility in file uploads</p>

    <p>Passing certain inputs to multipart forms could result in an infinite
    loop when parsing files.</p>
</li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:1.10.7-2+deb9u15.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2906.data"
# $Id: $

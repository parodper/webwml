#use wml::debian::translation-check translation="e5fb121d0e4a26b9278c1c164e88f200ab60497d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans isync, une
application de synchronisation de boîtes aux lettres IMAP et Maildir. Un
attaquant malveillant qui peut contrôler un serveur IMAP peut exploiter ces
défauts pour l'exécution de code à distance.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.2.1-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets isync.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de isync, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/isync">\
https://security-tracker.debian.org/tracker/isync</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3066.data"
# $Id: $

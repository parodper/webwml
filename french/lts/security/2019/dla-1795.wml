#use wml::debian::translation-check translation="8753221e322a5bd3df7ae5b05150e073d1f61042" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans graphicsmagick, la boîte
à outils de traitement d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11473">CVE-2019-11473</a>

<p>La fonction WriteMATLABImage (coders/mat.c) est sujette à un dépassement de
tampon basé sur le tas. Des attaquants distants peuvent exploiter cette
vulnérabilité pour provoquer un déni de service ou un autre impact non précisé
à l’aide de matrices contrefaites Matlab.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11474">CVE-2019-11474</a>

<p>La fonction WritePDBImage (coders/pdb.c) est sujette à un dépassement de
tampon basé sur le tas. Des attaquants distants peuvent exploiter cette
vulnérabilité pour provoquer un déni de service ou un autre impact non précisé
à l'aide d'un fichier de base de données contrefait Palm.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11505">CVE-2019-11505</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-11506">CVE-2019-11506</a>

<p>Le module XWD (coders/xwd.c) est sujet à plusieurs dépassements de tampon
basé sur le tas et d’exceptions arithmétiques. Des attaquants distants peuvent
exploiter ces divers défauts pour provoquer un déni de service ou un autre
impact non précisé à l’aide de fichiers XWD contrefaits.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.20-3+deb8u7.</p>
<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1795.data"
# $Id: $

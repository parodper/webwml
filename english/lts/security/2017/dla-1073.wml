<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in OpenJDK, an
implementation of the Oracle Java platform, resulting in sandbox bypass,
incorrect authentication, the execution of arbitrary code, denial of
service, information disclosure, use of insecure cryptography or
bypassing Jar verification.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7u151-2.6.11-1+deb7u1.</p>

<p>We recommend that you upgrade your openjdk-7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1073.data"
# $Id: $

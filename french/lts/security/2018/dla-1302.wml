#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Différents défauts ont été découverts dans leptonlib, une bibliothèque de
traitement d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7186">CVE-2018-7186</a>

<p>Leptonica ne limitait pas le nombre de caractères dans l’argument de
format %s pour fscanf ou sscanf, ce qui rendait possible pour des attaquants
distants de provoquer un déni de service (un dépassement de pile) ou
éventuellement d’avoir un impact non précisé à l'aide d'une longue chaîne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7440">CVE-2018-7440</a>

<p>La fonction gplotMakeOutput permettait une injection de commande à l'aide
d'une approche $(command) dans l’argument rootname de gplot. Ce problème existe
à cause d’un correctif incomplet pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-3836">CVE-2018-3836</a>.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.69-3.1+deb7u2.</p>


<p>Nous vous recommandons de mettre à jour vos paquets leptonlib.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1302.data"
# $Id: $

#use wml::debian::translation-check translation="846effa858f46cf6429f61e241ec96292032972f"
<define-tag pagetitle>Opdateret Debian 10: 10.5 udgivet</define-tag>
<define-tag release_date>2020-08-01</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den femte opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Punktopdateringen løser også Debian Security Advisory: 
<a href="https://www.debian.org/security/2020/dsa-4735">DSA-4735-1 grub2 -- 
sikkerhedsopdatering</a>, som dækker adskillige CVE-problemer vedrørende
the <a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/">\
Sårbarheden GRUB2 UEFI SecureBoot 'BootHole'</a>.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Opdatering af en eksisterende installation til denne revision, kan gøres ved 
at lade pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction appstream-glib 				"Retter opbygningsfejl i 2020 og senere">
<correction asunder 					"Anvender som standard gnudb i stedet for freedb">
<correction b43-fwcutter 				"Sikrer at fjernelse lykkes under ikke-engelske locales; lad ikke fjernelse fejle hvis nogle filer ikke længere findes; retter manglende afhængigheder af pciutils og ca-certificates">
<correction balsa 					"Leverer serveridentitet når der valideres certifikater, hvilket tillader succesfuld validering når der anvendes patch'en til glib-networking vedrørende CVE-2020-13645">
<correction base-files 					"Opdaterer til denne punktopdatering">
<correction batik 					"Retter forespørgselsforfalskning på server-side gennem xlink:href-attributter [CVE-2019-17566]">
<correction borgbackup 					"Retter indekskorruptionsfejl førende til datatab">
<correction bundler 					"Opdaterer krævet version af ruby-molinillo">
<correction c-icap-modules 				"Tilføjer understøttelse af ClamAV 0.102">
<correction cacti 					"Retter problem hvor UNIX-timestamps efter 13. september 2020 blev afvist som graph start / end; retter fjernudførelse af kode [CVE-2020-7237], udførelse af skripter på tværs af websteder [CVE-2020-7106], CSRF-problem [CVE-2020-13231]; deaktiverer af en brugerkonto medfører ikke at rettigheder omgående bliver ugyldige [CVE-2020-13230]">
<correction calamares-settings-debian 			"Aktiverer displaymanager-modul, retter autologin-valgmuligheder; anvender xdg-user-dir til at angive Desktop-mappe">
<correction clamav 					"Ny opstrømsudgave; sikkerhedsrettelser [CVE-2020-3327 CVE-2020-3341 CVE-2020-3350 CVE-2020-3327 CVE-2020-3481]">
<correction cloud-init 					"Ny opstrømsudgave">
<correction commons-configuration2 			"Forhændrer objektoprettelse ved indlæsning af YAML-filer [CVE-2020-1953]">
<correction confget 					"Retter Python-modulets håndtering af værdier indeholdende <q>=</q>">
<correction dbus 					"Ny stabil opstrømsudgave; forhindrer et lammelsesangrebsproblem [CVE-2020-12049]; forhindrer anvendelse efter frigivelse hvis to brugernavne deler en uid">
<correction debian-edu-config 				"Retter tab af dynamisk allokerede IPv4-adresser">
<correction debian-installer 				"Opdaterer Linux ABI til 4.19.0-10">
<correction debian-installer-netboot-images 		"Genopbygger mod proposed-updates">
<correction debian-ports-archive-keyring 		"Forøger 2020-nøglens udløbsdato (84C573CD4E1AFD6C) med et år; tilføjer Debian Ports Archive Automatic Signing Key (2021); flytter 2018-nøglen (ID: 06AED62430CB581C) til removed-keyring">
<correction debian-security-support 			"Opdaterer flere pakkers supportstatus">
<correction dpdk 					"Ny opstrømsudgave">
<correction exiv2 					"Justerer for restriktiv sikkerhedspatch [CVE-2018-10958 og CVE-2018-10999]; retter lammelsesangrebsproblem [CVE-2018-16336]">
<correction fdroidserver 				"Retter Litecoin-adressevalidering">
<correction file-roller 				"Sikkerhedsrettelse [CVE-2020-11736]">
<correction freerdp2 					"Retter smartcard-logins; sikkerhedsrettelser [CVE-2020-11521 CVE-2020-11522 CVE-2020-11523 CVE-2020-11524 CVE-2020-11525 CVE-2020-11526]">
<correction fwupd 					"Ny opstrømsudgave; retter mulige signaturverifikationsproblemer [CVE-2020-10759]; anvender roterede Debian-signeringsnøgler">
<correction fwupd-amd64-signed 				"Ny opstrømsudgave; retter mulige signaturverifikationsproblemer [CVE-2020-10759]; anvender roterede Debian-signeringsnøgler">
<correction fwupd-arm64-signed 				"Ny opstrømsudgave; retter mulige signaturverifikationsproblemer [CVE-2020-10759]; anvender roterede Debian-signeringsnøgler">
<correction fwupd-armhf-signed 				"Ny opstrømsudgave; retter mulige signaturverifikationsproblemer [CVE-2020-10759]; anvender roterede Debian-signeringsnøgler">
<correction fwupd-i386-signed 				"Ny opstrømsudgave; retter mulige signaturverifikationsproblemer [CVE-2020-10759]; anvender roterede Debian-signeringsnøgler">
<correction fwupdate 					"Anvender roterede Debian-signeringsnøgler">
<correction fwupdate-amd64-signed 			"Anvender roterede Debian-signeringsnøgler">
<correction fwupdate-arm64-signed 			"Anvender roterede Debian-signeringsnøgler">
<correction fwupdate-armhf-signed 			"Anvender roterede Debian-signeringsnøgler">
<correction fwupdate-i386-signed 			"Anvender roterede Debian-signeringsnøgler">
<correction gist 					"Undgår udfaset autorisations-API">
<correction glib-networking 				"Returnerer bad identity-fejl hvis identity ikke er opsat [CVE-2020-13645]; sørger for at balsa ældre end 2.5.6-2+deb10u1 fejler da rettelsen af CVE-2020-13645 får balsas certifikatverifikation til at fejle">
<correction gnutls28 					"Retter TL1.2-resumption-fejl; retter hukommelseslækage; håndterer nul længde-sessiontickets, retter forbindelsesfejl ved TLS1.2-sessioner til nogle store hostingproviders; retter verifikationsfejl med alternative chains">
<correction intel-microcode 				"Nedgraderer nogle microcodes til tidligere udgivne versioner, hvilket omgår hængende system ved boot på Skylake-U/Y og Skylake Xeon E3">
<correction jackson-databind 				"Retter adskillige sikkerhedsproblemer som påvirker BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 og CVE-2019-17267]">
<correction jameica 					"Tilføjer mckoisqldb til classpath, tillader brug af SynTAX-plugin">
<correction jigdo 					"Retter HTTPS-understøttelse i jigdo-lite og jigdo-mirror">
<correction ksh 					"Retter problem med begrænsning af miljøvariabel [CVE-2019-14868]">
<correction lemonldap-ng 				"Retter regression i nginx' opsætning, opstået med rettelsen af CVE-2019-19791">
<correction libapache-mod-jk 				"Omdøber Apaches opsætningsfil, så den automatisk kan aktiveres og deaktiveres">
<correction libclamunrar 				"Ny stabil opstrømsudgave; tilføjer en ikke-versioneret metapakke">
<correction libembperl-perl 				"Håndterer fejlsider fra Apache &gt;= 2.4.40">
<correction libexif 					"Sikkerhedsrettelser [CVE-2020-12767 CVE-2020-0093 CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; retter bufferoverløb [CVE-2020-0182] og heltalsoverløb [CVE-2020-0198]">
<correction libinput 					"Quirks: tilføjer trackpoint-integrationsattribut">
<correction libntlm 					"Retter bufferoverløb [CVE-2019-17455]">
<correction libpam-radius-auth 				"Retter bufferoverløb i password-felt [CVE-2015-9542]">
<correction libunwind 					"Retter segfaults på mips; aktiverer manuelt C++ exception-understøttelse kun på i386 og amd64">
<correction libyang 					"Retter cachekorruptionsnedbrud, CVE-2019-19333, CVE-2019-19334">
<correction linux 					"Ny stabil opstrømsudgave">
<correction linux-latest 				"Opdaterer til kerne-ABI 4.19.0-10">
<correction linux-signed-amd64 				"Ny stabil opstrømsudgave">
<correction linux-signed-arm64 				"Ny stabil opstrømsudgave">
<correction linux-signed-i386 				"Ny stabil opstrømsudgave">
<correction lirc 					"Retter håndtering af conffile">
<correction mailutils 					"maidag: dropper setuid-rettigheder for alle afleveringshandlinger bortset fra mda [CVE-2019-18862]">
<correction mariadb-10.3 				"Ny stabil opstrømsudgave; sikkerhedsrettelser [CVE-2020-2752 CVE-2020-2760 CVE-2020-2812 CVE-2020-2814 CVE-2020-13249]; retter regression i RocksDB ZSTD-genkendelse">
<correction mod-gnutls 					"Retter en mulig segfault ved mislykket TLS-handshake; retter testfejl">
<correction multipath-tools 				"kpartx: anvender korrekt sti til partx i udev-regel">
<correction mutt 					"Kontroller ikke IMAP PREAUTH-kryptering hvis $tunnel er i brug">
<correction mydumper 					"Linker mod libm">
<correction nfs-utils 					"statd: tager user-id fra /var/lib/nfs/sm [CVE-2019-3689]; overfør ikke hele ejerskabet af /var/lib/nfs til statd">
<correction nginx 					"Retter sårbarhed i forbindelse med fejlsideforespørgselssmugling [CVE-2019-20372]">
<correction nmap 					"Opdaterer standardnøglestørrelse til 2048 bits">
<correction node-dot-prop 				"Retter regression opstået i CVE-2020-8116 fix">
<correction node-handlebars 				"Tillader ikke direkte kald af <q>helperMissing</q> og <q>blockHelperMissing</q> [CVE-2019-19919]">
<correction node-minimist 				"Retter prototype-forurening [CVE-2020-7598]">
<correction nvidia-graphics-drivers 			"Ny stabil opstrømsudgave; sikkerhedsrettelser [CVE-2020-5963 CVE-2020-5967]">
<correction nvidia-graphics-drivers-legacy-390xx 	"Ny stabil opstrømsudgave; sikkerhedsrettelser [CVE-2020-5963 CVE-2020-5967]">
<correction openstack-debian-images 			"Installerer resolvconf hvis cloud-init installeres">
<correction pagekite 					"Undgår problemer med udløb af medfølgende SSL-certifikater ved at anvende dem fra pakken ca-certificates">
<correction pdfchain 					"Retter nedbrud ved start">
<correction perl 					"Retter adskillige regulære udtryk med forbindelse til sikkerhedsproblemer [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde 					"Retter sårbarhed i forbindelse med udførelse af skripter på tværs af websteder [CVE-2020-8035]">
<correction php-horde-gollem 				"Retter sårbarhed i forbindelse med udførelse af skripter på tværs af websteder i breadcrumb-uddata [CVE-2020-8034]">
<correction pillow 					"Retter adskillige problemer med læsning udenfor grænserne [CVE-2020-11538 CVE-2020-10378 CVE-2020-10177]">
<correction policyd-rate-limit 				"Retter problemer i opgørelse på grund af genbrugt socket">
<correction postfix 					"Ny stabil opstrømsudgave; retter segfault i tlsproxys client-rolle når serverrollen er deaktiveret; retter <q>standardværdi for maillog_file_rotate_suffix anvendte minuttet i stedet for måneden</q>; retter flere TLS-relaterede problemer; rettelser til README.Debian">
<correction python-markdown2 				"Retter problem med udførelse af skripter på tværs af websteder [CVE-2020-11888]">
<correction python3.7 					"Undgår uendelig løkke når der læses særligt fremstillede TAR-filer vha. tarfile-modulet [CVE-2019-20907]; løser hashkollisioner for IPv4Interface og IPv6Interface [CVE-2020-14422]; retter lammelsesangrebsproblem i urllib.request.AbstractBasicAuthHandler [CVE-2020-8492]">
<correction qdirstat 					"Retter gemning af brugeropsatte MIME-kategorier">
<correction raspi3-firmware 				"Retter slåfejl som kunne føre til systemer der ikke kan boote">
<correction resource-agents 				"IPsrcaddr: gør <q>proto</q> valgfri for at rette regression når anvendt uden NetworkManager">
<correction ruby-json 					"Retter usikker objektoprettelsessårbarhed [CVE-2020-10663]">
<correction shim 					"Anvender roterede Debian-signeringsnøgler">
<correction shim-helpers-amd64-signed 			"Anvender roterede Debian-signeringsnøgler">
<correction shim-helpers-arm64-signed 			"Anvender roterede Debian-signeringsnøgler">
<correction shim-helpers-i386-signed 			"Anvender roterede Debian-signeringsnøgler">
<correction speedtest-cli 				"Overfører korrekte headere for at rette test af uploadhastighed">
<correction ssvnc 					"Retter skrivning udenfor grænserne [CVE-2018-20020], uendelig løkke [CVE-2018-20021], ukorrekt initialisering [CVE-2018-20022], potentielt lammelsesangreb [CVE-2018-20024]">
<correction storebackup 				"Retter mulig rettighedsforøgelsesårbarhed [CVE-2020-7040]">
<correction suricata 					"Retter bortkastelse af rettigheder i nflog-runmode">
<correction tigervnc 					"Anvend ikke libunwind på armel, armhf og arm64">
<correction transmission 				"Retter muligt lammelsesangrebsproblem [CVE-2018-10756]">
<correction wav2cdr 					"Anvender C99-heltalstyper med fast størrelse for at rette runtime-assertion på andre 64 bit-arkitekturer end amd64 og alpha">
<correction zipios++ 					"Sikkerhedsrettelse [CVE-2019-13453]">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2020 4626 php7.3>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4679 keystone>
<dsa 2020 4680 tomcat9>
<dsa 2020 4681 webkit2gtk>
<dsa 2020 4682 squid>
<dsa 2020 4683 thunderbird>
<dsa 2020 4684 libreswan>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4690 dovecot>
<dsa 2020 4691 pdns-recursor>
<dsa 2020 4692 netqmail>
<dsa 2020 4694 unbound>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4696 nodejs>
<dsa 2020 4697 gnutls28>
<dsa 2020 4699 linux-signed-amd64>
<dsa 2020 4699 linux-signed-arm64>
<dsa 2020 4699 linux-signed-i386>
<dsa 2020 4699 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4707 mutt>
<dsa 2020 4708 neomutt>
<dsa 2020 4709 wordpress>
<dsa 2020 4710 trafficserver>
<dsa 2020 4711 coturn>
<dsa 2020 4712 imagemagick>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4714 chromium>
<dsa 2020 4716 docker.io>
<dsa 2020 4718 thunderbird>
<dsa 2020 4719 php7.3>
<dsa 2020 4720 roundcube>
<dsa 2020 4721 ruby2.5>
<dsa 2020 4722 ffmpeg>
<dsa 2020 4723 xen>
<dsa 2020 4724 webkit2gtk>
<dsa 2020 4725 evolution-data-server>
<dsa 2020 4726 nss>
<dsa 2020 4727 tomcat9>
<dsa 2020 4728 qemu>
<dsa 2020 4729 libopenmpt>
<dsa 2020 4730 ruby-sanitize>
<dsa 2020 4731 redis>
<dsa 2020 4732 squid>
<dsa 2020 4733 qemu>
<dsa 2020 4735 grub-efi-amd64-signed>
<dsa 2020 4735 grub-efi-arm64-signed>
<dsa 2020 4735 grub-efi-ia32-signed>
<dsa 2020 4735 grub2>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>			<th>Årsag</th></tr>
<correction golang-github-unknwon-cae 	"Sikkerhedsproblemer; vedligeholdes ikke">
<correction janus 			"Ikke mulig at understøtte i stable">
<correction mathematica-fonts 		"Afhængig af utilgængeligt downloadsted">
<correction matrix-synapse 		"Sikkerhedsproblemer; ikke mulig at understøtte">
<correction selenium-firefoxdriver 	"Ikke kompatibel med nyere versioner af Firefox ESR">
</table>


<h2>Debian Installer</h2>

<p>Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.</p>


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>

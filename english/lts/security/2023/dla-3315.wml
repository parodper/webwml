<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes multiple file format validation vulnerabilities that could
result in memory access violations such as buffer overflows and floating point
exceptions. It also fixes a regression in hcom parsing introduced when fixing
<a href="https://security-tracker.debian.org/tracker/CVE-2017-11358">CVE-2017-11358</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13590">CVE-2019-13590</a>

    <p>In <code>sox-fmt.h</code> (startread function), there is an integer overflow on the
    result of integer addition (wraparound to 0) fed into the <code>lsx_calloc</code> macro
    that wraps <code>malloc</code>. When a <code>NULL</code> pointer is returned, it is used without a
    prior check that it is a valid pointer, leading to a <code>NULL</code> pointer
    dereference on <code>lsx_readbuf</code> in <code>formats_i.c</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3643">CVE-2021-3643</a>

    <p>The <code>lsx_adpcm_init</code> function within libsox leads to a
    global-buffer-overflow. This flaw allows an attacker to input a malicious
    file, leading to the disclosure of sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23159">CVE-2021-23159</a>

    <p>A vulnerability was found in SoX, where a heap-buffer-overflow occurs
    in function <code>lsx_read_w_buf()</code> in <code>formats_i.c</code> file. The vulnerability is
    exploitable with a crafted file, that could cause an application to
    crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23172">CVE-2021-23172</a>

    <p>A vulnerability was found in SoX, where a heap-buffer-overflow occurs
    in function <code>startread()</code> in <code>hcom.c</code> file. The vulnerability is
    exploitable with a crafted hcomn file, that could cause an application
    to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23210">CVE-2021-23210</a>

    <p>A floating point exception (divide-by-zero) issue was discovered in
    SoX in functon <code>read_samples()</code> of <code>voc.c</code> file. An attacker with a
    crafted file, could cause an application to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33844">CVE-2021-33844</a>

    <p>A floating point exception (divide-by-zero) issue was discovered in
    SoX in functon <code>startread()</code> of <code>wav.c</code> file. An attacker with a crafted
    wav file, could cause an application to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40426">CVE-2021-40426</a>

    <p>A heap-based buffer overflow vulnerability exists in the <code>sphere.c</code>
    <code>start_read()</code> functionality of Sound Exchange libsox. A specially-crafted
    file can lead to a heap buffer overflow. An attacker can provide a
    malicious file to trigger this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31650">CVE-2022-31650</a>

    <p>There is a floating-point exception in <code>lsx_aiffstartwrite</code> in <code>aiff.c</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31651">CVE-2022-31651</a>

    <p>There is an assertion failure in <code>rate_init</code> in <code>rate.c</code>.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
14.4.2+git20190427-1+deb10u1.</p>

<p>We recommend that you upgrade your sox packages.</p>

<p>For the detailed security status of sox please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sox">https://security-tracker.debian.org/tracker/sox</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3315.data"
# $Id: $

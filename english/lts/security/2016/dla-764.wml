<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in QEMU:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9911">CVE-2016-9911</a>

    <p>Quick Emulator (Qemu) built with the USB EHCI Emulation support
    is vulnerable to a memory leakage issue. It could occur while
    processing packet data in <q>ehci_init_transfer</q>. A guest user/
    process could use this issue to leak host memory, resulting in
    DoS for a host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9921">CVE-2016-9921</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9922">CVE-2016-9922</a>

    <p>Quick emulator (Qemu) built with the Cirrus CLGD 54xx VGA Emulator
    support is vulnerable to a divide by zero issue. It could occur
    while copying VGA data when cirrus graphics mode was set to be
    VGA. A privileged user inside guest could use this flaw to crash
    the Qemu process instance on the host, resulting in DoS.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u19.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-764.data"
# $Id: $

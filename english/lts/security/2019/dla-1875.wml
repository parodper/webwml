<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In FusionDirectory, an LDAP web-frontend written in PHP (originally
derived GOsa² 2.6.x), a vulnerability was found that could theoretically
lead to unauthorized access to the LDAP database managed with
FusionDirectory. LDAP queries' result status ("Success") checks had not
been strict enough. The resulting output containing the word <q>Success</q>
anywhere in the returned data during login connection attempts would have
returned <q>LDAP success</q> to FusionDirectory and possibly grant unwanted
access.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.0.8.2-5+deb8u2.</p>

<p>We recommend that you upgrade your fusiondirectory packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1875.data"
# $Id: $

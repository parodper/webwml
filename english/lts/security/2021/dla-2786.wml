<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issue have been discovered in nghttp2: server, proxy and client
implementing HTTP/2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000168">CVE-2018-1000168</a>

    <p>An Improper Input Validation CWE-20 vulnerability found in ALTSVC frame handling
    that can result in segmentation fault leading to denial of service. This attack
    appears to be exploitable via network client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11080">CVE-2020-11080</a>

    <p>The overly large HTTP/2 SETTINGS frame payload causes denial of service.
    The proof of concept attack involves a malicious client constructing a SETTINGS
    frame with a length of 14,400 bytes (2400 individual settings entries) over and over again.
    The attack causes the CPU to spike at 100%.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.18.1-1+deb9u2.</p>

<p>We recommend that you upgrade your nghttp2 packages.</p>

<p>For the detailed security status of nghttp2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nghttp2">https://security-tracker.debian.org/tracker/nghttp2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2786.data"
# $Id: $

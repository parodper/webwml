#use wml::debian::translation-check translation="1d2fd1714f1d841afadf2605f0b229684d097166" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>L’annonce DLA 2589-1 corrigeait mal le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-26519">CVE-2020-26519</a>
et introduisait une régression lors de l’ouverture d’un document PDF,
aboutissant à un plantage SIGFPE, une erreur de nombre flottant.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.9a+ds1-4+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mupdf.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mupdf, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mupdf">\
https://security-tracker.debian.org/tracker/mupdf</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2589-2.data"
# $Id: $

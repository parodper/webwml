#use wml::debian::translation-check translation="ad23dfa1abd331efbc299f6c550d72ae2f406f29" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bookworm Alpha 2</define-tag>
<define-tag release_date>2023-02-19</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la deuxième version alpha de l’installateur pour Debian 12 <q>Bookworm</q>.
</p>

<p>
Suite à la
<a href="https://www.debian.org/vote/2022/vote_003">\
résolution générale de 2022 concernant les microprogrammes non libres</a>,
de nombreux paquets contenant des fichiers de microprogrammes qui peuvent être
requis par le noyau Linux au moment de son exécution ont été déplacés de la
section non-free à la section non-free-firmware, ce qui les rend éligibles à
l'inclusion sur les supports officiels conformément à l'article 5 du contrat
social de Debian.
</p>

<p>
À partir de cette publication, les images officielles incluent des paquets de
microprogrammes issus des sections main et non-free-firmware, en même temps que
les métadonnées pour configurer en conséquence le système installé. Le guide
d'installation
<a href="https://www.debian.org/releases/bookworm/amd64/ch02s02">\
a été mis à jour conformément</a>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>apt-setup :
  <ul>
    <li>ajout du message apt-setup/non-free-firmware, semblable aux messages
    pour non-free et contrib, utilisé par 50mirror pour activer le
    composant non-free-firmware : installation experte uniquement, désactivé par
    défaut, mais défini automatiquement quand des paquets de microprogrammes
    sont installés ;</li>
    <li>factorisation de la gestion des composants dans tous les générateurs
    de Debian et ajout de la prise en charge de non-free-firmware pour
    chacun d'entre eux ;</li>
    <li>plus de duplication des entrées cdrom: dans les commentaires à la
    fin de l'installation (<a href="https://bugs.debian.org/1029922">nº 1029922</a>).</li>
  </ul>
  </li>
  <li>base-installer :
  <ul>
    <li>qualification multi-arch ignoré (<a href="https://bugs.debian.org/1020426">nº 1020426</a>).</li>
  </ul>
  </li>
  <li>cdebconf :
  <ul>
    <li>text : durant la progression, fourniture d'un retour pour l'utilisateur
    au moins toutes les minutes ;</li>
    <li>newt : plus d'éléments alignés dans le coin supérieur gauche en mode
    braille.</li>
  </ul>
  </li>
  <li>debian-cd :
  <ul>
    <li>informations modalias rafraîchies pour firmware-sof-signed ;</li>
    <li>extension des métadonnées générées sur les microprogrammes avec des
    informations sur les composants ;</li>
    <li>inclusion des paquets de microprogrammes basée sur les composants
    configurés (par exemple main et non-free-firmware pour les constructions
    officielles) ;</li>
    <li>passage à 1 Go de la taille maximale des images de CD d'installation
    par le réseau : 650 Mo ne sont plus suffisants, mais la taille des images
    actuelles reste en dessous de 700 Mo ;</li>
    <li>implémentation d'une recherche plus robuste de l'installateur lors
    d'un amorçage à partir de grub-efi, en utilisant .disk/id/$UUID à la place
    de .disk/info (<a href="https://bugs.debian.org/1024346">nº 1024346</a>,
    <a href="https://bugs.debian.org/1024720">nº 1024720</a>) ;</li>
    <li>toutes les images d'installation amd64 démarrent maintenant sur les
    machines utilisant un microprogramme UEFI 32 bits. Les utilisateurs n'ont
    plus besoin de l'installateur multi-architecture pour ces machines ;</li>
    <li>arrêt de la construction d'images multi-architectures en conséquence ;</li>
    <li>suppression de la prise en charge de win32-loader qui n'est plus
    entretenu.</li>
  </ul>
  </li>
  <li>debian-installer :
  <ul>
    <li>passage à l'utilisation du thème de Bookworm pour les écrans de
    démarrage (Emerald) ;</li>
    <li>Correction de l'absence de glyphes pour le géorgien (<a href="https://bugs.debian.org/1017435">nº 1017435</a>) ;</li>
    <li>passage de l'ABI du noyau Linux à la version 6.1.0-3.</li>
  </ul>
  </li>
  <li>debootstrap :
  <ul>
    <li>ajout d'un traitement particulier pour le paquet usr-is-merged.</li>
  </ul>
  </li>
  <li>espeakup :
  <ul>
    <li>réglage du paramètre direct speakup_soft à 1 dans les systèmes
    installés pour les langues autres que l'anglais, pour corriger la
    prononciation des symboles et des chiffres ;</li>
    <li>plus d'avertissement sur l'absence des voix de mbrola ;</li>
    <li>exécution d'espeakup dans une boucle, pour compenser les plantages.</li>
  </ul>
  </li>
  <li>grub-installer :
  <ul>
    <li>correction de l'absence d'intégration du module debconf.</li>
  </ul>
  </li>
  <li>grub2 :
  <ul>
    <li>ajout d'une section commentée de GRUB_DISABLE_OS_PROBER à
    /etc/default/grub pour faciliter pour les utilisateurs la remise en marche
    d'os-prober s'ils le souhaitent (<a href="https://bugs.debian.org/1013797">nº 1013797</a>,
    <a href="https://bugs.debian.org/1009336">nº 1009336</a>).</li>
  </ul>
  </li>
  <li>hw-detect :
  <ul>
    <li>ajout de la prise en charges des index Contents-firmware pour
    accélérer la recherche de microprogrammes (des fichiers requis vers les
    paquets disponibles), en conservant la recherche manuelle comme repli ;</li>
    <li>ajout de la prise en charge de la détection des composants, en activant
    automatiquement apt-setup/$component ;</li>
    <li>résolution du module « usb » dans le module sous-jacent quand il
    nécessite des fichiers de microprogrammes (par exemple rtl8192cu pour une
    clé wifi USB RTL8188CUS), pour recharger le module correct après le
    déploiement du paquet de microprogrammes adéquat ;</li>
    <li>suppression de la prise en charge du chargement des paquets de
    microprogrammes de udeb (*.udeb, *.ude) ;</li>
    <li>Note : les cas d'utilisation du chargement de microprogrammes à partir
    d'un support de stockage externe reste à clarifier (<a href="https://bugs.debian.org/1029543">nº 1029543</a>). Avec l'inclusion des paquets de microprogrammes
    dans les images officielles, il est attendu que cela soit beaucoup moins
    utile qu'auparavant ;</li>
    <li>le va-et-vient historique « link up/link down » évité dans certaines
    interfaces réseau : bien que ce soit utile pour s'assurer que les modules
    requièrent les fichiers de microprogrammes dont ils pourraient avoir besoin,
    c'est réellement nuisible si l'interface a été configurée (par exemple
    manuellement ou par préconfiguration) : le lien est actif et/ou est impliqué
    dans une liaison ;</li>
    <li>implémentation de la prise en charge du réglage hw-detect/firmware-lookup=never
    (<a href="https://bugs.debian.org/1029848">nº 1029848</a>).</li>
  </ul>
  </li>
  <li>libdebian-installer :
  <ul>
    <li>suffixe qualificatif multiarch ignoré (<a href="https://bugs.debian.org/1020783">nº 1020783</a>).</li>
  </ul>
  </li>
  <li>localechooser :
  <ul>
    <li>activation du vietnamien dans la console non bogl.</li>
  </ul>
  </li>
  <li>ifupdown :
  <ul>
    <li>correction de l'absence de configuration de /etc/network/interfaces
    pour les connexions sans fil quand NetworkManager n'est pas installé,
    modifiant ses permissions à 600 pour des connexions sûres dans la mesure
    où il termine en renfermant des secrets
    (<a href="https://bugs.debian.org/1029352">nº 1029352</a>, premier problème) ;</li>
    <li>ajustement de la configuration de /etc/network/interfaces pour les
    connexions sans fil avec à la fois DHCP et SLAAC : écriture uniquement
    d'un paragraphe DHCP et les Router Advertisements font le reste au moment
    de l'exécution
    (<a href="https://bugs.debian.org/1029352">nº 1029352</a>, second problème).</li>
  </ul>
  </li>
  <li>preseed :
  <ul>
    <li>ajout de l'alias « firmware » pour « hw-detect/firmware-lookup »
    (<a href="https://bugs.debian.org/1029848">nº 1029848</a>).</li>
  </ul>
  </li>
  <li>rootskel :
  <ul>
    <li>reopen-console-linux : quand speakup est requis, ouverture en
    priorité de tty1, la seule console que speakup peut lire.</li>
  </ul>
  </li>
  <li>rootskel-gtk :
  <ul>
    <li>Apparence graphique mise à jour avec le thème Emerald.
    </li>
  </ul>
  </li>
  <li>user-setup :
  <ul>
    <li>retrait de la prise en charge des mots de passe non cachés.</li>
  </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
  <ul>
    <li>[amd64, arm64] construction d'images netboot pour les matériels ChromeOS.
    Des modifications supplémentaires sont nécessaires pour rendre ces images
    efficaces.</li>
  </ul>
  </li>
  <li>grub2 :
  <ul>
    <li>ajout de smbios aux images grub-efi signées (<a href="https://bugs.debian.org/1008106">nº 1008106</a>) ;</li>
    <li>ajout de serial aux images grub-efi signées (<a href="https://bugs.debian.org/1013962">nº 1013962</a>) ;</li>
    <li>Pas de retrait des symboles des binaires de Xen, afin qu'ils fonctionnent
     à nouveau (<a href="https://bugs.debian.org/1017944">nº 1017944</a>) ;</li>
    <li>activation de la prise en charge de EFI zboot dans arm64 (<a href="https://bugs.debian.org/1026092">nº 1026092</a>) ;</li>
    <li>certains nouveaux attributs de ext2 ignorés pour conserver la compatibilité
    avec les attributs par défaut des dernières versions de mke2fs (<a href="https://bugs.debian.org/1030846">nº 1030846</a>).</li>
  </ul>
  </li>
  <li>linux :
  <ul>
    <li>udeb : déplacement de ledtrig-audio de sound-modules vers
    kernel-image</li>
    <li>udeb : ajout également de pilotes dans les sous-répertoires de
    drivers/net/phy</li>
    <li>[arm64] ajout du pilote nvmem-imx-ocotp au paquet kernel-image ;</li>
    <li>[arm64] ajout du pilote imx2_wdt au paquet udeb kernel-image ;</li>
    <li>[arm64] ajout du pilote i2c-imx au paquet udeb i2c-modules.</li>
  </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour 40 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>

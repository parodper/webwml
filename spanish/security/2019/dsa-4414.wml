#use wml::debian::translation-check translation="49d6d85a79874ccc5eece0c873e13c73f733ac07"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varios problemas en el módulo de Apache auth_mellon, que
proporciona autenticación SAML 2.0.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3877">CVE-2019-3877</a>

    <p>Era posible sortear la comprobación de URL redirigida en logout, de forma que
    el módulo podía usarse como una utilidad de redirección abierta.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3878">CVE-2019-3878</a>

    <p>Al usar mod_auth_mellon en una configuración de Apache como
    proxy remoto con el módulo http_proxy, era
    posible sortear la autenticación mediante el envío de cabeceras SAML ECP.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 0.12.0-2+deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de libapache2-mod-auth-mellon.</p>

<p>Para información detallada sobre el estado de seguridad de libapache2-mod-auth-mellon,
consulte su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon">\
https://security-tracker.debian.org/tracker/libapache2-mod-auth-mellon</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4414.data"

#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu-kvm, une solution
complète de virtualisation des hôtes Linux sur du matériel x86 avec des
clients x86.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3710">CVE-2016-3710</a>

<p>Wei Xiao et Qinghao Tang de 360.cn Inc ont découvert un défaut de
lecture et d'écriture hors limites dans le module VGA de QEMU. Un
utilisateur client privilégié pourrait utiliser ce défaut pour exécuter du
code arbitraire sur l'hôte avec les privilèges du processus hôte de QEMU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3712">CVE-2016-3712</a>

<p>Zuozhi Fzz de Alibaba Inc a découvert de possibles problèmes de
dépassement d'entier ou d'accès en lecture hors limites dans le module VGA
de QEMU. Un utilisateur client privilégié pourrait utiliser ce défaut pour
monter un déni de service (plantage du processus de QEMU).</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.1.2+dfsg-6+deb7u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu-kvm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-539.data"
# $Id: $

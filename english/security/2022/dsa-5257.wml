<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4037">CVE-2021-4037</a>

    <p>Christian Brauner reported that the inode_init_owner function for
    the XFS filesystem in the Linux kernel allows local users to create
    files with an unintended group ownership allowing attackers to
    escalate privileges by making a plain file executable and SGID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0171">CVE-2022-0171</a>

    <p>Mingwei Zhang reported that a cache incoherence issue in the SEV API
    in the KVM subsystem may result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1184">CVE-2022-1184</a>

    <p>A flaw was discovered in the ext4 filesystem driver which can lead
    to a use-after-free. A local user permitted to mount arbitrary
    filesystems could exploit this to cause a denial of service (crash
    or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2602">CVE-2022-2602</a>

    <p>A race between handling an io_uring request and the Unix socket
    garbage collector was discovered. An attacker can take advantage of
    this flaw for local privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2663">CVE-2022-2663</a>

    <p>David Leadbeater reported flaws in the nf_conntrack_irc
    connection-tracking protocol module. When this module is enabled
    on a firewall, an external user on the same IRC network as an
    internal user could exploit its lax parsing to open arbitrary TCP
    ports in the firewall, to reveal their public IP address, or to
    block their IRC connection at the firewall.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3061">CVE-2022-3061</a>

    <p>A flaw was discovered in the i740 driver which may result in denial
    of service.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3176">CVE-2022-3176</a>

    <p>A use-after-free flaw was discovered in the io_uring subsystem which
    may result in local privilege escalation to root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3303">CVE-2022-3303</a>

    <p>A race condition in the snd_pcm_oss_sync function in the sound
    subsystem in the Linux kernel due to improper locking may result in
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-20421">CVE-2022-20421</a>

    <p>A use-after-free vulnerability was discovered in the
    binder_inc_ref_for_node function in the Android binder driver. On
    systems where the binder driver is loaded, a local user could
    exploit this for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39188">CVE-2022-39188</a>

    <p>Jann Horn reported a race condition in the kernel's handling of
    unmapping of certain memory ranges. When a driver created a
    memory mapping with the VM_PFNMAP flag, which many GPU drivers do,
    the memory mapping could be removed and freed before it was
    flushed from the CPU TLBs. This could result in a page use-after-free.
    A local user with access to such a device could exploit
    this to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39842">CVE-2022-39842</a>

    <p>An integer overflow was discovered in the pxa3xx-gcu video driver
    which could lead to a heap out-of-bounds write.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40307">CVE-2022-40307</a>

    <p>A race condition was discovered in the EFI capsule-loader driver,
    which could lead to use-after-free. A local user permitted to
    access this device (/dev/efi_capsule_loader) could exploit this to
    cause a denial of service (crash or memory corruption) or possibly
    for privilege escalation. However, this device is normally only
    accessible by the root user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41674">CVE-2022-41674</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-42719">CVE-2022-42719</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-42720">CVE-2022-42720</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-42721">CVE-2022-42721</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-42722">CVE-2022-42722</a>

    <p>Soenke Huster discovered several vulnerabilities in the mac80211
    subsystem triggered by WLAN frames which may result in denial of
    service or the execution or arbitrary code.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.149-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5257.data"
# $Id: $

#use wml::debian::template title="Documentación obsoleta"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"
#use wml::debian::translation-check translation="4792632f7a20682a368627c66663b57ff1b3fa8b"

<h1 id="historical">Documentos históricos</h1>

<p>Los documentos listados a continuación, bien fueron escritos hace mucho tiempo y
no están al día, o bien se han escrito para versiones anteriores de Debian
y no se han actualizado para las versiones actuales. La información que contienen está
desactualizada, pero todavía puede ser de interés para algunos.</p>

<p>Los documentos que han perdido relevancia y ya no sirven a ningún propósito
han sido eliminados de aquí, pero el código fuente de muchos de estos manuales
obsoletos puede encontrarse en 
<a href="https://salsa.debian.org/ddp-team/attic">el ático del Proyecto de Documentación de Debian</a>.</p>


<h2 id="user">Documentación orientada al usuario</h2>

<document "dselect Documentación para principiantes" "dselect">

<div class="centerblock">
<p>
  Este fichero documenta «dselect» para nuevos usuarios con la intención de ayudar
  a instalar Debian con éxito. No intenta explicarlo
  todo, así que, cuando utilice «dselect» por primera vez, consulte las pantallas de ayuda.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  congelado: <a href="https://packages.debian.org/aptitude">aptitude</a> ha
  sustituido a «dselect» como interfaz estándar de gestión de paquetes de Debian.
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk vcstype="attic"">
  </availability>
</doctable>
</div>

<hr />

<document "Guía de usuario («User's Guide»)" "users-guide">

<div class="centerblock">
<p>
Esta <q>guía de usuario</q> no es más que una <q>guía de usuario de Progeny</q> reformateada.
Su contenido está adaptado al sistema Debian estándar.</p>

<p>Más de 300 páginas con un buen tutorial para empezar a utilizar el sistema Debian
tanto desde la <acronym lang="es" title="Interfaz gráfica de usuario («Graphical User Interface»)">GUI</acronym> de escritorio
como desde línea de órdenes.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Útil en sí mismo como tutorial. Fue escrito para la versión woody
  y se ha quedado obsoleto.
  </status>
  <availability>
# langs="en" no es redundante, añade el sufijo de idioma necesario al enlace
  <inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Tutorial de Debian («Debian Tutorial»)" "tutorial">

<div class="centerblock">
<p>
Este manual es para nuevos usuarios de Linux, para ayudarles a conocerlo
una vez lo han instalado, o bien para nuevos usuarios de Linux en un sistema
administrado por otra persona.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  congelado; incompleto;
  probablemente obsoleto tras la <a href="user-manuals#quick-reference">Guía de referencia de Debian</a>.
  </status>
  <availability>
  no completo aún
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Guía de instalación y uso («Debian GNU/Linux: Guide to Installation and Usage»)" "guide">

<div class="centerblock">
<p>
  Un manual orientado al usuario final.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  listo (pero es para potato).
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Manual de referencia del usuario de Debian («Debian User Reference Manual»)" "userref">

<div class="centerblock">
<p>
  Este manual proporciona cuando menos una visión general de todo lo que un usuario debería saber
  sobre el sistema Debian GNU/Linux (por ejemplo: configuración de X, cómo configurar
  la red, acceso a discos flexibles, etc.). Ha sido escrito con la intención de cubrir la brecha
  entre el tutorial de Debian y las detalladas páginas de manual e «info»
  proporcionadas con cada paquete.</p>

  <p>También tiene la intención de dar algunas ideas sobre cómo combinar órdenes, basándose en
  el principio general de Unix de que <em>siempre hay más de una forma de
  hacerlo</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  congelado y bastante incompleto;
  probablemente obsoleto tras la <a href="user-manuals#quick-reference">Guía de referencia de Debian</a>.
  </status>
  <availability>
  <inddpvcs name="user" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />


<document "Manual del administrador del sistema Debian («Debian System Administrator's Manual»)" "system">

<div class="centerblock">
<p>
  En la introducción del manual de normas («Policy manual») se menciona este documento.
  Cubre todos los aspectos de administración de un sistema Debian.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  congelado; incompleto;
  probablemente obsoleto tras la <a href="user-manuals#quick-reference">Guía de referencia de Debian</a>.
  </status>
  <availability>
  no disponible todavía.
  <inddpvcs name="system-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Manual del administrador de red con Debian («Debian Network Administrator's Manual»)" "network">

<div class="centerblock">
<p>
  Este manual cubre todos los aspectos de administración de redes en un sistema Debian.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  congelado; incompleto;
  probablemente obsoleto tras la <a href="user-manuals#quick-reference">Guía de referencia de Debian</a>.
  </status>
  <availability>
  no disponible todavía.
  <inddpvcs name="network-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

# Añadir este a la relación de libros, hay una edición revisada (segunda) en Amazon
<document "El libro de recetas de Linux («The Linux Cookbook»)" "linuxcookbook">

<div class="centerblock">
<p>
  Una guía de referencia práctica del sistema Debian GNU/Linux que muestra,
  a través de más de 1.500 <q>recetas</q>, cómo usarlo para actividades de cada día (desde
  trabajar con texto, imágenes y sonido hasta cuestiones de productividad y
  redes). El propio libro se distribuye con «copyleft», igual que el software que describe,
  y su código fuente está disponible.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  publicado; fue escrito para woody y se ha quedado obsoleto
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">del autor</a>
  </availability>
</doctable>
</div>

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
  Este manual quiere ser una fuente de información rápida, pero completa,
  sobre el sistema APT y sus características. Contiene mucha información
  sobre los usos principales de APT y muchos ejemplos.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  obsoleto desde 2009
  </status>
  <availability>
  <inoldpackage "apt-howto">
  <inddpvcs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	    formats="html txt pdf ps" naming="locale" vcstype="attic"/>
  </availability>
</doctable>
</div>


<h2 id="devel">Documentación para desarrolladores</h2>

<document "Introducción: hacer un paquete Debian («Introduction: Making a Debian Package»)" "makeadeb">

<div class="centerblock">
<p>
  Introducción a cómo construir un <code>.deb</code> usando
  <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  congelado, obsoleto tras la <a href="devel-manuals#maint-guide">Guía del nuevo desarrollador de Debian</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML en línea</a>
  </availability>
</doctable>
</div>

<hr />

<document "Manual de programadores Debian («Debian Programmers' Manual»)" "programmers">

<div class="centerblock">
<p>
  Ayuda a los nuevos desarrolladores a construir un paquete para el sistema Debian GNU/Linux.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  obsoleto tras la <a href="devel-manuals#maint-guide">Guía del nuevo desarrollador de Debian</a>.
  </status>
  <availability>
  <inddpvcs name="programmer" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Manual del autor de paquetes Debian («Debian Packaging Manual»)" "packman">

<div class="centerblock">
<p>
  Este manual describe los aspectos técnicos de la construcción de paquetes
  binarios y fuentes. También documenta la interfaz entre «dselect»
  y sus «scripts» de métodos de acceso. No trata los requisitos normativos
  del proyecto Debian, y asume familiaridad con las funciones de
  «dpkg» desde la perspectiva del administrador del sistema.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Partes que eran normas de facto se integraron en 
  el <a href="devel-manuals#policy">Manual de normas de Debian («debian-policy»)</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<document "Cómo pueden distribuir los Productores de Software sus
productos directamente en formato .deb" "swprod">

<div class="centerblock">
<p>
  Este documento está pensado como un punto de inicio al explicar cómo
  los productores de software pueden integrar sus productos con Debian,
  qué situaciones pueden surgir dependiendo de la licencia de
  los productos, las opciones de los productores y las posibilidades que
  tienen. No explica cómo crear paquetes, pero enlaza a documentos que
  hacen justo eso.

  <p>Debería leer esto si no está en general familiarizado con la
  creación y distribución de paquetes para Debian y, opcionalmente,
  con su adición a la distribución.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  Disponible (?)
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr />

<document "Introducción a la i18n" "i18n">

<div class="centerblock">
<p>
  Este documento explica a los programadores y mantenedores de paquetes
  la idea básica y <q>cómo hacer</q> l10n (localización), i18n
  (internacionalización) y m17n (multilingualización).

  <p>El objetivo de este documento es hacer que haya más paquetes con
  soporte de i18n y conseguir que Debian sea una distribución más
  internacionalizada. Son bienvenidas las contribuciones de cualquier
  parte del mundo, ya que el autor original es japonés y su
  documento sería sobre <q>japonización</q> si nadie contribuyera.

<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  parado, obsoleto
  </status>
  <availability>
  sin completar aún
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr />

<document "Cómo de Debian sobre SGML/XML («Debian SGML/XML HOWTO»)" "sgml-howto">

<div class="centerblock">
<p>
  Este «Cómo» contiene información práctica sobre el uso de SGML y XML
  en un sistema operativo Debian.

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  parado, obsoleto
  </status>
  <availability>

# English only using index.html, so langs set.
  <inddpvcs name="sgml-howto"
            formats="html"
            srctype="SGML"
            vcstype="attic"
/>
  </availability>
</doctable>
</div>

<hr />

<document "Normativa para Debian XML/SGML" "xml-sgml-policy">

<div class="centerblock">
<p>
  Subnorma para paquetes de Debian que proporcionen o hagan uso de
  recursos XML o SGML.

<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  Empezando, fusionando la norma actual sobre SGML de
  <tt>sgml-base-doc</tt> y nuevo material para la gestión del catálogo XML
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<hr>

<document "Manual de marcas de DebianDoc-SGML" "markup">

<div class="centerblock">
<p>
  Documentación para el sistema <strong>debiandoc-sgml</strong>,
  incluyendo consejos para los mantenedores. Las próximas versiones
  deberían incluir consejos para un mejor mantenimiento y procesado de
  documentación en paquetes Debian, directivas para organizar la
  traducción de la documentación y otras informaciones de interés.
  Véase también <a href="https://bugs.debian.org/43718">bug #43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  listo
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>

<h2 id="misc">Documentación miscelánea</h2>

<document "Guía de repositorios Debian («Debian Repository HOWTO»)" "repo">

<div class="centerblock">
<p>
  Este documento explica cómo funcionan los repositorios Debian, cómo crearlos
  y cómo incluirlos correctamente en <tt>sources.list</tt>.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  listo (?)
  </status>
  <availability>
  <inddpvcs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta" vcstype="attic">
  </availability>
</doctable>
</div>

